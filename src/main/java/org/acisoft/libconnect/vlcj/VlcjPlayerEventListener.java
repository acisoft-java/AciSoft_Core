package org.acisoft.libconnect.vlcj;

import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventAdapter;

public class VlcjPlayerEventListener extends MediaPlayerEventAdapter {
    @Override
    public void finished(MediaPlayer mediaPlayer) {
        mediaPlayer.stop();
    }
}

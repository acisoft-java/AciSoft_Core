package org.acisoft.libconnect.vlcj;

import org.acisoft.core.player.Player;
import org.acisoft.core.PlayState;
import org.acisoft.core.Time;
import org.acisoft.core.playable.MediaType;
import org.acisoft.core.playable.Playable;
import org.acisoft.exception.IllegalStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.caprica.vlcj.player.MediaPlayer;

import java.util.Optional;

public class VlcjAudioVideoPlayer implements Player {
	private static final Logger LOGGER = LoggerFactory.getLogger(VlcjAudioVideoPlayer.class);

	protected MediaPlayer player;
	private Playable loadedPlayable;

	/**
	 * Flag used to work around the issue of vlc(j) not using a STOPPED state.
	 */
	private boolean stopped;

	public VlcjAudioVideoPlayer(MediaPlayer player) {
		this.player = player;
	}

	@Override
	public void dispose() {
		this.stop();
		this.player.release();
	}

	@Override
	public void play() {
		this.player.play();
		this.stopped = false;
	}

	@Override
	public void play(Playable playable) {
		if (this.supportsPlaybackOf(playable)) {
			this.player.playMedia(playable.getAbsoluteFilePath().get());
			this.loadedPlayable = playable;
			this.stopped = false;
		} else {
			logPlaybackNotSupportedFor(playable);
		}
	}

	@Override
	public boolean canPlay() {
		return (this.getLoadedPlayable() != null && this.player.isPlayable());
	}

	@Override
	public void pause() {
		PlayState state = PlayState.IDLE;
		try {
			state = this.getPlayState();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}

		if (this.canPause() && state != PlayState.PAUSED) this.player.pause();
	}

	@Override
	public void playPause() {
		try {
			if (this.getPlayState() == PlayState.PLAYING) {
				if (this.canPause()) this.pause();
			} else if (this.canPlay()) {
				this.play();
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return;
		}
	}

	@Override
	public boolean canPause() {
		return (this.getLoadedPlayable() != null && this.player.canPause());
	}

	@Override
	public void stop() {
		this.player.stop();
		this.stopped = true;
	}

	@Override
	public boolean canStop() {
		return !stopped;
	}

	@Override
	public void load(Playable playable) {
		if (supportsPlaybackOf(playable)) {
			this.player.prepareMedia(playable.getAbsoluteFilePath().get());
			this.loadedPlayable = playable;
		} else {
			logPlaybackNotSupportedFor(playable);
		}
	}

	@Override
	public Optional<Playable> getLoadedPlayable() {
		return Optional.ofNullable(this.loadedPlayable);
	}

	@Override
	public boolean hasLoadedPlayable() {
		return this.loadedPlayable != null;
	}

	@Override
	public boolean supportsPlaybackOf(Playable playable) {
		return playable.hasSourceFile() && (playable.getType() == MediaType.AUDIO || playable.getType() == MediaType.VIDEO);
	}

	@Override
	public PlayState getPlayState() throws IllegalStateException {
		if (this.loadedPlayable == null) return PlayState.IDLE;
		else if (this.stopped) return PlayState.STOPPED;

		switch (this.player.getMediaPlayerState()) {
			case libvlc_Playing:
			case libvlc_Buffering:
			case libvlc_Opening:
				return PlayState.PLAYING;

			case libvlc_Paused:
				return PlayState.PAUSED;

			case libvlc_Stopped:
			case libvlc_Ended:
				return PlayState.STOPPED;

			case libvlc_Error:
				LOGGER.error("[AciSoft's VlcjAudioVideoPlayer Unit] libvlc is currently in an error state!");
			case libvlc_NothingSpecial:
				return PlayState.IDLE;

			default:
				LOGGER.error("libvlc is currently in an unhandled state: {}!",
                        player.getMediaPlayerState());
				throw new IllegalStateException("libvlcj is in an unkown state!");
		}
	}

	@Override
	public boolean isPlaying() {
		PlayState state = null;
		try {
			state = this.getPlayState();
		} catch (IllegalStateException e) {
			return false;
		}

		return (state == PlayState.PLAYING);
	}

	@Override
	public boolean isPaused() {
		PlayState state = null;
		try {
			state = this.getPlayState();
		} catch (IllegalStateException e) {
			return false;
		}

		return (state == PlayState.PAUSED);
	}

	@Override
	public boolean isStopped() {
		PlayState state = null;
		try {
			state = this.getPlayState();
		} catch (IllegalStateException e) {
			return false;
		}

		return (state == PlayState.IDLE || state == PlayState.STOPPED);
	}

	@Override
	public Time getTime() {
		return Time.fromMilliseconds((int) this.player.getTime());
	}

	@Override
	public void setTime(Time time) {
		this.player.setTime(time.toMilliseconds());
	}

	@Override
	public int getVolume() {
		return this.player.getVolume();
	}

	@Override
	public void setVolume(int volume) {
		this.player.setVolume(volume);
	}

	protected void logPlaybackNotSupportedFor(Playable playable) {
		LOGGER.warn("Playback of {} not supported", playable);
	}
}
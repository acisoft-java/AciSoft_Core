package org.acisoft.libconnect.vlcj.finder;

import org.acisoft.libconnect.vlcj.finder.strategy.VlcFinderStrategy;

import java.io.File;
import java.util.List;
import java.util.Optional;

public class DefaultVlcFinder implements VlcFinder {
    private List<VlcFinderStrategy> strategiesToBeUsed;

    public DefaultVlcFinder(List<VlcFinderStrategy> strategiesToBeUsed) {
        this.strategiesToBeUsed = strategiesToBeUsed;
    }

    @Override
    public Optional<File> findVlc() {
        for (VlcFinderStrategy vlcFinderStrategy : strategiesToBeUsed) {
            Optional<File> vlcInstallationDirectory = vlcFinderStrategy.findVlc();
            if (vlcInstallationDirectory.isPresent()) {
                return vlcInstallationDirectory;
            }
        }

        return Optional.empty();
    }
}

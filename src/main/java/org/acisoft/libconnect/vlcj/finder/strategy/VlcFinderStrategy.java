package org.acisoft.libconnect.vlcj.finder.strategy;

import java.io.File;
import java.util.Optional;

public interface VlcFinderStrategy {
    Optional<File> findVlc();
}

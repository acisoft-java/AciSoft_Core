package org.acisoft.libconnect.vlcj.finder.strategy;

import java.io.File;
import java.util.Optional;

public class DirectoryVlcFinderStrategy implements VlcFinderStrategy {
    private File directory;

    public DirectoryVlcFinderStrategy(String directoryPath) {
        this(new File(directoryPath));
    }

    public DirectoryVlcFinderStrategy(File directory) {
        this.directory = directory;
    }

    @Override
    public Optional<File> findVlc() {
        return (directory.exists() && directory.isDirectory() && hasFileWithNameContainingVlc(directory)) ?
                Optional.of(directory) : Optional.empty();
    }

    public boolean hasFileWithNameContainingVlc(File directory) {
        for (File file : directory.listFiles()) {
            if (file.isFile() && file.getName().toLowerCase().contains("vlc")) {
                return true;
            }
        }

        return false;
    }
}

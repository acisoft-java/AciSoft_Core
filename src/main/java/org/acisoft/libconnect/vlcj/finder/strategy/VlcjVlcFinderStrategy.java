package org.acisoft.libconnect.vlcj.finder.strategy;

import uk.co.caprica.vlcj.discovery.NativeDiscoveryStrategy;
import uk.co.caprica.vlcj.discovery.linux.DefaultLinuxNativeDiscoveryStrategy;
import uk.co.caprica.vlcj.discovery.mac.DefaultMacNativeDiscoveryStrategy;
import uk.co.caprica.vlcj.discovery.windows.DefaultWindowsNativeDiscoveryStrategy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class VlcjVlcFinderStrategy implements VlcFinderStrategy {
    private List<NativeDiscoveryStrategy> vlcjNativeStrategies;

    public VlcjVlcFinderStrategy() {
        this.vlcjNativeStrategies = new ArrayList<>();
        addDefaultStrategies();
    }

    private void addDefaultStrategies() {
        vlcjNativeStrategies.add(new DefaultLinuxNativeDiscoveryStrategy());
        vlcjNativeStrategies.add(new DefaultWindowsNativeDiscoveryStrategy());
        vlcjNativeStrategies.add(new DefaultMacNativeDiscoveryStrategy());
    }

    @Override
    public Optional<File> findVlc() {
        for (NativeDiscoveryStrategy vlcjNativeStrategy : vlcjNativeStrategies) {
            String foundPath = vlcjNativeStrategy.discover();
            if (foundPath != null) {
                return Optional.of(new File(foundPath));
            }
        }

        return Optional.empty();
    }
}

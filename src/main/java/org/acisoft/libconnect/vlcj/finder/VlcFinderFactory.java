package org.acisoft.libconnect.vlcj.finder;

import org.acisoft.libconnect.vlcj.finder.strategy.DirectoryVlcFinderStrategy;
import org.acisoft.libconnect.vlcj.finder.strategy.VlcFinderStrategy;
import org.acisoft.libconnect.vlcj.finder.strategy.VlcjVlcFinderStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class VlcFinderFactory {

    public VlcFinder searchSpecificDirectoriesAndThenVlcjFinder(List<String> directoriesToCheck) {
        List<VlcFinderStrategy> strategiesToBeUsed = new ArrayList<>();

        strategiesToBeUsed.addAll(toVlcFinderStrategyList(directoriesToCheck));
        strategiesToBeUsed.addAll(defaultStrategies());

        return new DefaultVlcFinder(strategiesToBeUsed);
    }

    private List<DirectoryVlcFinderStrategy> toVlcFinderStrategyList(List<String> directoriesToCheck) {
        return directoriesToCheck.stream()
                .map(DirectoryVlcFinderStrategy::new)
                .collect(Collectors.toList());
    }

    private List<VlcFinderStrategy> defaultStrategies() {
        return asList(
                new VlcjVlcFinderStrategy()
        );
    }
}

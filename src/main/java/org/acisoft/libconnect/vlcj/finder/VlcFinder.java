package org.acisoft.libconnect.vlcj.finder;

import java.io.File;
import java.util.Optional;

public interface VlcFinder {
    Optional<File> findVlc();
}

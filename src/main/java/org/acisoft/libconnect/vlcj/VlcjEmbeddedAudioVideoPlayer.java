package org.acisoft.libconnect.vlcj;

import org.acisoft.core.player.EmbeddedAudioVideoPlayer;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;

import java.awt.*;

public class VlcjEmbeddedAudioVideoPlayer extends VlcjAudioVideoPlayer implements EmbeddedAudioVideoPlayer {
	private Canvas jCanvas;

    public VlcjEmbeddedAudioVideoPlayer(EmbeddedMediaPlayer embeddedMediaPlayer, Canvas videoCanvas) {
		super(embeddedMediaPlayer);

		if (videoCanvas == null) throw new NullPointerException("'videoCanvas' cannot be null!");
		this.jCanvas = videoCanvas;
	}

	@Override
	public Canvas getCanvas() {
		return jCanvas;
	}
}

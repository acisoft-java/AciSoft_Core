package org.acisoft.libconnect.vlcj;

import org.acisoft.core.player.Player;
import org.acisoft.core.player.EmbeddedAudioVideoPlayer;
import org.acisoft.core.player.PlayerFactory;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import uk.co.caprica.vlcj.player.embedded.videosurface.CanvasVideoSurface;

import java.awt.*;

public final class VlcjMediaPlayerFactory implements PlayerFactory {

	private MediaPlayerFactory vlcjMediaPlayerFactory;

	public VlcjMediaPlayerFactory() {
		this.vlcjMediaPlayerFactory = new MediaPlayerFactory();
	}

	@Override
	public EmbeddedAudioVideoPlayer newEmbeddedPlayer(Canvas screen) {
		EmbeddedMediaPlayer embeddedMediaPlayer = vlcjMediaPlayerFactory.newEmbeddedMediaPlayer();

		CanvasVideoSurface videoSurface = vlcjMediaPlayerFactory.newVideoSurface(screen);
		embeddedMediaPlayer.setVideoSurface(videoSurface);

		embeddedMediaPlayer.addMediaPlayerEventListener(new VlcjPlayerEventListener());

		return new VlcjEmbeddedAudioVideoPlayer(embeddedMediaPlayer, screen);
	}

	@Override
	public Player newHeadlessPlayer() {
		return new VlcjAudioVideoPlayer(this.vlcjMediaPlayerFactory.newHeadlessMediaPlayer());
	}

	@Override
	public void dispose() {
		this.vlcjMediaPlayerFactory.release();
	}
}

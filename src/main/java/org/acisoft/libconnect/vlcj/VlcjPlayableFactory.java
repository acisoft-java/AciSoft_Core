package org.acisoft.libconnect.vlcj;

import org.acisoft.core.PlayableFactory;
import org.acisoft.core.Time;
import org.acisoft.core.playable.AudioFilePlayable;
import org.acisoft.core.playable.MediaType;
import org.acisoft.core.playable.Playable;
import org.acisoft.core.playable.VideoFilePlayable;
import org.acisoft.exception.UnsupportedFileException;
import uk.co.caprica.vlcj.filter.*;
import uk.co.caprica.vlcj.player.MediaMeta;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;

import java.io.File;
import java.util.Optional;

public class VlcjPlayableFactory implements PlayableFactory {

	private MediaFileFilter mediaFileFilter;
	private ExtensionFileFilter audioExtFilter;
	private ExtensionFileFilter videoExtFilter;
	private ExtensionFileFilter playlistExtFilter;
    private ExtensionFileFilter subtitleExtFilter;

	private MediaPlayer player;

	public VlcjPlayableFactory() {
		this.mediaFileFilter = new MediaFileFilter();
		this.audioExtFilter = new AudioFileFilter();
		this.videoExtFilter = new VideoFileFilter();
		this.playlistExtFilter = new PlayListFileFilter();
		this.subtitleExtFilter = new SubTitleFileFilter();

		String[] args = { "--vout", "dummy" };
		this.player = (new MediaPlayerFactory(args)).newHeadlessMediaPlayer();
		this.player.mute(true);
	}

    @Override
    public boolean accepts(File file) {
        return videoExtFilter.accept(file) || audioExtFilter.accept(file);
    }

    @Override
	public Playable createFrom(File file) {
        MediaType type = determineMediaType(file);
        Optional<Time> length = determineMediaLengthMillis(file)
                .map(Long::intValue)
                .map(Time::fromMilliseconds);

		if (type == MediaType.VIDEO) {
			return length.map(time -> new VideoFilePlayable(file, time))
                    .orElseGet(() -> new VideoFilePlayable(file));
		} else {
            return length.map(time -> new AudioFilePlayable(file, time))
                    .orElseGet(() -> new AudioFilePlayable(file));
		}
	}

    private Optional<Long> determineMediaLengthMillis(File file) {
        this.player.prepareMedia(file.getAbsolutePath());
        this.player.parseMedia();

        MediaMeta meta = null;
        long len;
        try {
            meta = this.player.getMediaMeta();
            len = meta.getLength();
        } finally {
            if (meta != null) {
                meta.release();
            }
        }

        return len > 0 ? Optional.of(len) : Optional.empty();
    }

    private MediaType determineMediaType(File file) {
        MediaType type;
        if (videoExtFilter.accept(file)) {
            type = MediaType.VIDEO;
        } else if (audioExtFilter.accept(file)) {
            type = MediaType.AUDIO;
        } else {
            throw new UnsupportedFileException("The given file is not a video nor audio file, or is unsupported.");
        }
        return type;
    }

    @Override
	public void dispose() {
		this.player.release();
	}
}

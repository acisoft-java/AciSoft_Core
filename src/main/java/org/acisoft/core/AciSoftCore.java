package org.acisoft.core;

import java.io.File;
import java.net.URISyntaxException;

import org.acisoft.core.player.PlayerFactory;
import org.acisoft.exception.VlcNotFoundException;
import org.acisoft.exception.IllegalStateException;
import org.acisoft.exception.IncompatibleBitArchitectureException;

public abstract class AciSoftCore {

	protected PlayerFactory playerFactory;
	protected AciPlayer player;
	protected PlayableFactory playableFactory;

	protected AciSoftCore() {}

	/**
	 * <p>
	 * Boots the core.
	 * </p>
	 * 
	 * @throws VlcNotFoundException
	 *             When the core cannot locate the vlc directory
	 */
	public abstract void boot() throws IncompatibleBitArchitectureException, VlcNotFoundException;

	public abstract void stop();

	/**
	 * Get the {@link PlayerFactory}.
	 * 
	 * @return The {@link PlayerFactory}
	 */
	protected final PlayerFactory getPlayerFactory() {
		return this.playerFactory;
	}

	/**
	 * Sets the {@link PlayerFactory} to use.
	 * 
	 * @throws IllegalStateException
	 *             When the {@link PlayerFactory} has already been set before
	 */
	protected final void setPlayerFactory(PlayerFactory playerFactory) throws IllegalStateException {
		if (this.playerFactory != null)
			throw new IllegalStateException("Cannot set PlayerFactory any more: it has already been set.");
		else if (playerFactory == null)
			throw new NullPointerException("'playerFactory' cannot be null, but null given!");

		this.playerFactory = playerFactory;
	}

	/**
	 * Get the created {@link AciPlayer}.
	 * 
	 * @return The {@link AciPlayer}
	 */
	public final AciPlayer getPlayer() {
		return player;
	}

	/**
	 * Set the player to return.
	 * 
	 * @param player
	 *            The player to return
	 */
	protected final void setPlayer(AciPlayer player) {
		this.player = player;
	}

	/**
	 * Get the {@link PlayableFactory}.
	 * 
	 * @return The {@link PlayableFactory}
	 */
	public final PlayableFactory getPlayableFactory() {
		return this.playableFactory;
	}

	/**
	 * Set the {@link PlayableFactory}.
	 */
	protected final void setPlayableFactory(PlayableFactory playableFactory) {
		this.playableFactory = playableFactory;
	}

	/**
	 * Tries to get the JAR-file of the currently running program. If there's no JAR-file it will return the active directory.
	 * @return The JAR-file, or the active directory on failure.
	 */
	public final File getJarFile() {
		try {
			return new File(AciSoftCore.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
		} catch (URISyntaxException e) {
			return null;
		}
	}
	/*
	 * Add an error listening system that gets called when an error occurs in
	 * the Player System?
	 */
}

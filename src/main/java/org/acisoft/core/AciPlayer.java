package org.acisoft.core;

import org.acisoft.core.playable.Playable;
import org.acisoft.core.playable.customplayable.CustomVisual;
import org.acisoft.core.player.AciSoftEmbeddedMediaPlayer;
import org.acisoft.core.player.Player;
import org.acisoft.core.player.PlayerFactory;
import org.acisoft.exception.IllegalStateException;
import org.eclipse.swt.widgets.Monitor;

import java.util.Optional;

/**
 * The main player class of AciSoft.<br />
 *
 * Internally, it uses other (simple) player units.
 * 
 * @author regapictures
 */
public class AciPlayer {
    private AciSoftEmbeddedMediaPlayer player;

	public AciPlayer(PlayerFactory playerFactory, PlayerWindowOptions playerWindowOptions) {
        this.player = new AciSoftEmbeddedMediaPlayer(playerFactory, true, playerWindowOptions);
	}

	/**
	 * Free all resources used by this {@link AciPlayer}.
	 */
	public void dispose() {
		player.dispose();
	}

    /**
	 * Start playing the currently loaded {@link Playable}, if any. <br />
	 * Else if a {@link Playlist} is set, start playing the {@link Playable}
	 * at the Playlist's pointer.
	 */
	public void play() {
		if (this.hasLoadedPlayable()) this.player.play();
		/*
		 * else play playlist, if any
		 */
	}

	/**
	 * Start playing the given {@link Playable}.
	 * 
	 * @param playable
	 *            The {@link Playable} to play
	 */
	public void play(Playable playable) {
		player.play(playable);
	}

	/**
	 * Start playing the first {@link Playable} in the given {@link Playlist}.
	 * 
	 * @param p
	 *            The {@link Playlist} to play
	 */
	public void playPlaylist(Playlist p) {
		return; // TODO [Playlist] Not yet implemented
	}

	/**
	 * Start playing the {@link Playable} in the given {@link Playlist}, on
	 * the given index.
	 * 
	 * @param p
	 * @param mediaIndex
	 */
	public void playPlaylist(Playlist p, int mediaIndex) {
		return; // TODO [Playlist] Not yet implemented
	}

	/**
	 * Checks if calling {@link #play()} will succeed, and playback will start.
	 * 
	 * @return If calling {@link #play()} will succeed
	 */
	public boolean canPlay() {
		return this.canPlay();
	}

	/**
	 * Pause the playback.
	 */
	public void pause() {
		if (this.canPause()) this.player.pause();
	}

	/**
	 * Toggle play/pause.
	 */
	public void playPause() {
		this.player.playPause();
	}

	/**
	 * Checks if calling {@link #pause()} will succeed, and playback can be
	 * paused.
	 * 
	 * @return If calling {@link #pause()} will succeed
	 */
	public boolean canPause() {
		return this.player.canPause();
	}

	/**
	 * Stops the playback, and resets {@link AciPlayer} time to zero.
	 */
	public void stop() {
		if (this.canStop()) this.player.stop();
	}

	/**
	 * Checks if calling {@link #stop()} will succeed, and playback can be
	 * stopped.
	 * 
	 * @return If calling {@link #stop()} will succeed
	 */
	public boolean canStop() {
		return this.player.canStop();
	}

	/**
	 * Loads the given {@link Playable}, but doesn't start playing it.
	 * 
	 * @param playable
	 *            The {@link Playable} to load
	 */
	public void load(Playable playable) {
		player.load(playable);
	}

	/**
	 * Returns the current loaded {@link Playable}, if any. Otherwise, returns
	 * <code>null</code>.
	 * 
	 * @return The current loaded {@link Playable}, or <code>null</code> when
	 *         nothing's loaded
	 */
	public Optional<Playable> getLoadedPlayable() {
		return this.player.getLoadedPlayable();
	}

	/**
	 * Returns whether any playable is currently loaded.
	 * 
	 * @return Whether any playable is currently loaded
	 */
	public boolean hasLoadedPlayable() {
		return this.player.hasLoadedPlayable();
	}

	/**
	 * Returns the current {@link PlayState}.
	 * 
	 * @return The current {@link PlayState}
	 * @throws IllegalStateException
	 *             When the underlying playback library is in an unknown state
	 *             (that is not programmatically expected to occur). This could
	 *             occur for example when a library update introduces a new
	 *             (unknown) play state.
	 */
	public PlayState getPlayState() throws IllegalStateException {
		return this.player.getPlayState();
	}

	/**
	 * Returns whether this {@link Player} is playing.
	 * 
	 * @return Whether this {@link Player} is playing
	 */
	public boolean isPlaying() {
		return this.player.isPlaying();
	}

	/**
	 * Returns whether this {@link Player} is paused.
	 * 
	 * @return Whether this {@link Player} is paused
	 */
	public boolean isPaused() {
		return this.player.isPaused();
	}

	/**
	 * Returns whether this {@link Player} is stopped or idle.
	 * 
	 * @return Whether this {@link Player} is stopped or idle
	 */
	public boolean isStopped() {
		return this.player.isStopped();
	}

	/**
	 * Returns the current playback {@link Time} of the current playing
	 * {@link Playable}.
	 * 
	 * @return The current playback {@link Time} of the current playing
	 *         {@link Playable}
	 */
	public Time getTime() {
		return this.player.getTime();
	}

	/**
	 * Sets the current playback {@link Time} of the current playing
	 * {@link Playable}.
	 * 
	 * @param t
	 *            The playback {@link Time} to set
	 */
	public void setTime(Time t) {
		this.player.setTime(t);
	}

    /**
     * Gets the custom visual to be shown when no video is playing.
     */
	public Optional<CustomVisual> getIdleCustomVisual() {
	    return this.player.getIdleCustomVisual();
    }

    /**
     * Sets the given {@link CustomVisual} to be shown when nothing is playing.
     * @param entry The entry to be shown
     */
	public void setIdleCustomVisual(CustomVisual entry) {
	    this.player.setIdleCustomVisual(entry);
    }

    /**
     * Removes any {@link CustomVisual} set to be shown. Instead, a blank screen will be seen again.
     */
    public void removeIdleCustomVisual() {
	    this.player.removeIdleCustomVisual();
    }

	/**
	 * Sets the {@link Playlist} to use during playback.
	 * 
	 * @param p
	 *            The {@link Playlist} to use during playback
	 */
	public void setPlaylist(Playlist p) {
		return; // TODO [Playlist] Not yet implemented
	}

	/**
	 * Returns the current set {@link Playlist} being used during playback.
	 * 
	 * @return The current set {@link Playlist} being used during playback
	 */
	public Playlist getPlaylist() {
		return null; // TODO [Playlist] Not yet implemented
	}

	public void showWindow() {
		player.showWindow();
	}

	public void hideWindow() {
	    player.hideWindow();
    }

	public boolean isWindowVisible() {
		return this.player.isWindowVisible();
	}

	public Optional<Monitor> getAvailableTargetMonitorForWindow() {
	    return player.getAvailableTargetMonitorForWindow();
    }
}

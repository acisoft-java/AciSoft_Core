package org.acisoft.core;

public interface Updateable {
	void onUpdate();
}

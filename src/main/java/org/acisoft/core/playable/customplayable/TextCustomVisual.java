package org.acisoft.core.playable.customplayable;

import lombok.Getter;
import org.acisoft.core.playable.MediaType;
import org.acisoft.core.playable.customplayable.text.ParagraphWrapper;
import org.acisoft.core.playable.customplayable.text.WrappedParagraph;

import java.awt.*;
import java.io.File;
import java.util.Optional;

@Getter
public class TextCustomVisual extends CustomVisual {
    private String text;
    private int fontSize;
    private WrappingWidthPercentage wrappingWidthPercentage;

    public TextCustomVisual(String text, int fontSize, WrappingWidthPercentage wrappingWidthPercentage) {
        super(text);
        this.text = text;
        this.fontSize = fontSize;
        this.wrappingWidthPercentage = wrappingWidthPercentage;
    }

    @Override
    public void drawOn(Dimension size, Graphics2D graphics2D) {
        setupTextLayout(graphics2D);

        if (!text.isEmpty()) {
            final int midPointX = size.width / 2;
            final int midPointY = size.height / 2;
            final ParagraphWrapper paragraphWrapper = new ParagraphWrapper(getWrappingWidth(size),
                    graphics2D.getFont(), graphics2D.getFontRenderContext());
            final WrappedParagraph paragraph = paragraphWrapper.wrap(text);
            paragraph.drawCenteredAround(graphics2D, new Point(midPointX, midPointY));
        }
    }

    @Override
    public MediaType getType() {
        return MediaType.CUSTOM_VISUAL;
    }

    @Override
    public Optional<File> getSourceFile() {
        return Optional.empty();
    }

    private int getWrappingWidth(Dimension size) {
        return (int) Math.round(size.width * wrappingWidthPercentage.getScale());
    }

    private void setupTextLayout(Graphics2D graphics2D) {
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
        graphics2D.setPaint(Color.WHITE);
        graphics2D.setFont(new Font("Sansserif", Font.PLAIN, fontSize));
    }

    public static class WrappingWidthPercentage {
        private int value;

        public WrappingWidthPercentage(int value) {
            checkValue(value);
            this.value = value;
        }

        private void checkValue(int value) {
            if (value < 1 || value > 100) {
                throw new IllegalArgumentException("Value should be 1-100, but given: " + value);
            }
        }

        public double getScale() {
            return ((double) value) / 100;
        }
    }

}

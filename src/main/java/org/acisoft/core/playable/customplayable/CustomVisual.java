package org.acisoft.core.playable.customplayable;

import org.acisoft.core.playable.Playable;

import java.awt.*;

public abstract class CustomVisual extends Playable {
    public CustomVisual(String displayName) {
        super(displayName);
    }

    public abstract void drawOn(Dimension size, Graphics2D graphics2D);
}

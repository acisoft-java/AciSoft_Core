package org.acisoft.core.playable.customplayable;

import org.acisoft.core.playable.MediaType;
import org.acisoft.exception.UnsupportedFileException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

import static org.acisoft.core.Utils.fitACompletelyInB;

public class Image extends CustomVisual {
    private File fileReference;
    private BufferedImage image;

    public Image(File fileReference, BufferedImage bufferedImage) {
        super(fileReference.getName());
        this.fileReference = fileReference;
        this.image = bufferedImage;
    }

    public static Image load(File file) throws IOException, UnsupportedFileException {
        BufferedImage bufferedImage = ImageIO.read(file);
        if (bufferedImage == null) {
            throw new UnsupportedFileException();
        }

        return new Image(file, bufferedImage);
    }

    @Override
    public Optional<File> getSourceFile() {
        return Optional.of(fileReference);
    }

    @Override
    public void drawOn(Dimension size, Graphics2D graphics2D) {
        Dimension imageDimension = new Dimension(image.getWidth(), image.getHeight());
        Rectangle scaledRectangle = fitACompletelyInB(imageDimension, size);

        graphics2D.drawImage(image,
                scaledRectangle.x, scaledRectangle.y, scaledRectangle.width, scaledRectangle.height,
                (img, infoflags, x, y, width, height) -> false);
    }

    @Override
    public MediaType getType() {
        return MediaType.CUSTOM_VISUAL;
    }
}

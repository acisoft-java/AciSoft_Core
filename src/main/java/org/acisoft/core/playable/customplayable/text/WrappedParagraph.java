package org.acisoft.core.playable.customplayable.text;

import lombok.AccessLevel;
import lombok.Getter;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@Getter
public class WrappedParagraph {
    @Getter(value = AccessLevel.NONE)
    private List<MeasuredText> textLines;
    private int wrappingWidth = 0;
    private double height = 0;

    public WrappedParagraph(List<MeasuredText> textLines, int wrappingWidth) {
        this.wrappingWidth = wrappingWidth;
        this.textLines = textLines;
        height = calculateHeight();
    }

    public WrappedParagraph mergeWith(WrappedParagraph wrappedParagraph) {
        List<MeasuredText> lines = new ArrayList<>(textLines);
        lines.addAll(wrappedParagraph.textLines);
        return new WrappedParagraph(lines, wrappingWidth);
    }

    public double calculateHeight() {
        double height = 0;
        for (int i = 0; i < textLines.size(); i++) {
            MeasuredText measuredText = textLines.get(i);
            if (i > 0) {
                height += measuredText.spacingAbove;
            }

            height += measuredText.height;
        }

        return height;
    }

    public List<PositionedMeasuredText> calculateLinePositionsCenteredAround(Point position) {
        List<PositionedMeasuredText> positionedTextLines = new ArrayList<>();

        double paragraphOffsetX = position.x - wrappingWidth / 2;
        double paragraphOffsetY = position.y - height / 2;

        double offsetY = paragraphOffsetY;
        for (int i = 0; i < textLines.size(); i++) {
            MeasuredText measuredText = textLines.get(i);

            double offsetX = paragraphOffsetX + (wrappingWidth - measuredText.width) / 2;
            if (i > 0) {
                offsetY += measuredText.spacingAbove;
            }

            positionedTextLines.add(PositionedMeasuredText.from(measuredText, offsetX, offsetY));

            offsetY += measuredText.height;
        }

        return positionedTextLines;
    }

    public void drawCenteredAround(Graphics2D graphics2D, Point position) {
        List<PositionedMeasuredText> linePositions = calculateLinePositionsCenteredAround(position);
        for (PositionedMeasuredText linePosition : linePositions) {
            int offsetX = (int) Math.round(linePosition.offsetX);
            int offsetY = (int) Math.round(linePosition.offsetY + linePosition.ascent);

            graphics2D.drawString(linePosition.text, offsetX, offsetY);
        }
    }

}

package org.acisoft.core.playable.customplayable.text;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
@Getter
public class MeasuredText {
    public String text;
    public double width;
    public double height;
    public double spacingAbove;
    public double ascent;
}

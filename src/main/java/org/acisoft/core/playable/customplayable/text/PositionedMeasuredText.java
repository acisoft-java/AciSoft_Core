package org.acisoft.core.playable.customplayable.text;

import lombok.Getter;

@Getter
class PositionedMeasuredText extends MeasuredText {
    public double offsetX;
    public double offsetY;

    public PositionedMeasuredText(String text, double width, double height, double betweenLinesSpacing, double ascent,
                                  double offsetX, double offsetY) {
        super(text, width, height, betweenLinesSpacing, ascent);
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    public static PositionedMeasuredText from(MeasuredText measuredText, double offsetX, double offsetY) {
        return new PositionedMeasuredText(measuredText.text, measuredText.width, measuredText.height,
                measuredText.spacingAbove, measuredText.ascent, offsetX, offsetY);
    }
}

package org.acisoft.core.playable.customplayable.text;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.List;

// TODO Clean up. It's kinda dirty since I've added in multiline support quickly
public class    ParagraphWrapper {
    private int wrappingWidth;
    private Font font;
    private FontRenderContext fontRenderContext;

    public ParagraphWrapper(int wrappingWidth, Font font, FontRenderContext fontRenderContext) {
        this.wrappingWidth = wrappingWidth;
        this.font = font;
        this.fontRenderContext = fontRenderContext;
    }

    public WrappedParagraph wrap(String text) {
        WrappedParagraph wrappedParagraph = null;
        for (String line : text.split("\n")) {
            WrappedParagraph wrPar = new InternalWrapper(line).wrap();
            wrappedParagraph = (wrappedParagraph == null ? wrPar : wrappedParagraph.mergeWith(wrPar));
        }

        return wrappedParagraph;
    }

    private class InternalWrapper {
        private String text;

        public InternalWrapper(String text) {
            this.text = text;
        }

        private List<MeasuredText> textLines = new ArrayList<>();

        private LineBreakMeasurer measurer;
        private TextLayout currentLayout;
        private Interval currentCharIndexInterval = new Interval(0, 0);
        private String currentTextLine;

        public WrappedParagraph wrap() {
            measurer = getLineBreakMeasurer();

            while (wrappingNotComplete()) {
                nextLine();

                double lineWidth = currentLayout.getVisibleAdvance();
                double lineHeight = currentLayout.getAscent() + currentLayout.getDescent();

                MeasuredText measuredText = new MeasuredText(currentTextLine, lineWidth, lineHeight,
                        currentLayout.getLeading(), currentLayout.getAscent());
                textLines.add(measuredText);
            }

            return new WrappedParagraph(textLines, wrappingWidth);
        }

        private boolean wrappingNotComplete() {
            return measurer.getPosition() < text.length();
        }

        private void nextLine() {
            currentLayout = measurer.nextLayout(wrappingWidth);
            currentCharIndexInterval = currentCharIndexInterval.nextAbuttingRangeTo(measurer.getPosition());
            currentTextLine = text.substring(currentCharIndexInterval.fromInclusive, currentCharIndexInterval.toExclusive);
        }

        private LineBreakMeasurer getLineBreakMeasurer() {
            AttributedString attributedString = new AttributedString(text);
            attributedString.addAttribute(TextAttribute.FONT, font);
            AttributedCharacterIterator iterator = attributedString.getIterator();
            return new LineBreakMeasurer(iterator, fontRenderContext);
        }
    }

    public static class Interval {
        public int fromInclusive;
        public int toExclusive;

        public Interval(int fromInclusive, int toExclusive) {
            this.fromInclusive = fromInclusive;
            this.toExclusive = toExclusive;

            if (toExclusive < fromInclusive) {
                throw new IllegalArgumentException(toString() + " is not a valid open interval!");
            }
        }

        public Interval nextAbuttingRangeTo(int toExclusive) {
            return new Interval(this.toExclusive, toExclusive);
        }

        @Override
        public String toString() {
            return "[" + fromInclusive + ", " + toExclusive + "[";
        }
    }
}

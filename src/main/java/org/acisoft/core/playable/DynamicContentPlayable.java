package org.acisoft.core.playable;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.acisoft.core.Time;

import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public abstract class DynamicContentPlayable extends Playable {
    protected Time length;

    public DynamicContentPlayable(String displayName) {
        super(displayName);
    }

    public DynamicContentPlayable(String displayName, Time length) {
        super(displayName);
        this.length = length;
    }

    /**
     * @return Length of this entry, if known.
     */
    public Optional<Time> getLength() {
        return Optional.ofNullable(this.length);
    }

    public void setLength(Time t) {
        this.length = t;
    }
}

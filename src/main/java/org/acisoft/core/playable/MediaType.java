package org.acisoft.core.playable;

public enum MediaType {
	VIDEO, AUDIO, CUSTOM_VISUAL
}

package org.acisoft.core.playable;

import org.acisoft.core.PlayState;
import org.acisoft.core.player.Player;
import org.acisoft.core.Time;
import org.acisoft.exception.IllegalStateException;

import java.util.Optional;

public class NOPPlayer implements Player {
    @Override
    public void dispose() { }

    @Override
    public void play() { }

    @Override
    public void play(Playable playable) { }

    @Override
    public boolean canPlay() {
        return false;
    }

    @Override
    public void pause() { }

    @Override
    public void playPause() { }

    @Override
    public boolean canPause() {
        return false;
    }

    @Override
    public void stop() { }

    @Override
    public boolean canStop() {
        return false;
    }

    @Override
    public void load(Playable playable) { }

    @Override
    public Optional<Playable> getLoadedPlayable() {
        return Optional.empty();
    }

    @Override
    public boolean hasLoadedPlayable() {
        return false;
    }

    @Override
    public boolean supportsPlaybackOf(Playable playable) {
        return false;
    }

    @Override
    public PlayState getPlayState() throws IllegalStateException {
        return PlayState.STOPPED;
    }

    @Override
    public boolean isPlaying() {
        return false;
    }

    @Override
    public boolean isPaused() {
        return false;
    }

    @Override
    public boolean isStopped() {
        return true;
    }

    @Override
    public Time getTime() {
        return Time.ZERO;
    }

    @Override
    public void setTime(Time time) { }

    @Override
    public int getVolume() {
        return 0;
    }

    @Override
    public void setVolume(int volume) { }
}

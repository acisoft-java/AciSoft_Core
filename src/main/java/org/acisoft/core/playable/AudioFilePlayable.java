package org.acisoft.core.playable;

import java.io.File;
import java.util.Optional;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.acisoft.core.Time;

@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class AudioFilePlayable extends DynamicContentPlayable {
	private File file;

	public AudioFilePlayable(File file) {
		super(file.getName());
		this.file = file;
	}

	public AudioFilePlayable(File file, Time time) {
		super(file.getName(), time);
		this.file = file;
	}

	@Override
	public MediaType getType() {
		return MediaType.AUDIO;
	}

	@Override
	public Optional<File> getSourceFile() {
		return Optional.of(file);
	}
}
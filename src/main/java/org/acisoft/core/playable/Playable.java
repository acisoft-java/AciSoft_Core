package org.acisoft.core.playable;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.File;
import java.util.Optional;

@AllArgsConstructor
@Getter
public abstract class Playable {
	private String displayName;

	public abstract MediaType getType();
	public abstract Optional<File> getSourceFile();

	public Optional<String> getAbsoluteFilePath() {
		return getSourceFile().map(File::getAbsolutePath);
	}

	public boolean hasSourceFile() {
		return getSourceFile().isPresent();
	}

	@Override
	public String toString() {
		return "Playable{" +
				"displayName='" + displayName + "\'," +
				"type='" + getType() + "\'," +
				"sourceFile='" + getSourceFile() + "\'," +
				'}';
	}
}

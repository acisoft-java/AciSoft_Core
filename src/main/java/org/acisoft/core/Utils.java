package org.acisoft.core;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.function.Predicate;

public final class Utils {
	private Utils() {
		// Disable constructor //
	}

	/**
	 * Checks whether the given values is in the given range (min <= val <=
	 * max).
	 * 
	 * @param val
	 *            The value that needs to be checked
	 * @param min
	 *            The minimum value
	 * @param max
	 *            The maximum value
	 * @return Whether the given values is in the given range
	 * @throws IllegalArgumentException
	 *             When the minimum is greater than the maximum
	 */
	public static boolean valueInRange(long val, long min, long max) {
		if (min > max) throw new IllegalArgumentException(
				"Minimum should be less than or equal to maximum! Given: min=" + min + " > max= " + max);
		return (val >= min && val <= max);
	}

	/**
	 * Rounds the specified value with an accuracy of the specified amount of
	 * decimals.
	 * 
	 * @param value
	 *            The value to round
	 * @param decimals
	 *            The accuracy in amount of decimals
	 * @return The rounded number
	 */
	public static double round(double value, int decimals) {
		double multiplier = Math.pow(10, decimals);

		return Math.round(value * multiplier) / multiplier;
	}

	/**
	 * Returns whether the given number is a round number
	 * 
	 * @param num
	 * @return
	 */
	public static boolean isRound(double num) {
		return (num == ((long) num));
	}

	/**
	 * Prefixes the given number with zero's so the number has the minimum given
	 * string/display length.
	 * 
	 * @param number
	 *            The number to prefix
	 * @param minimumLength
	 *            The minimum string/display length
	 * @param includeMinus
	 *            Whether to include the minus of the number for the length
	 *            calculation
	 * @return The prefixed number, as a string
	 */
	public static String zeroPrefixNumber(long number, int minimumLength, boolean includeMinus) {
		// Is the number negative?
		boolean isNegative = (number < 0);
		// If so, make it positive
		if (isNegative) number = -number;

		// Get string
		String strNum = String.valueOf(number);
		// Get string length
		int length = strNum.length() + ((isNegative && includeMinus) ? 1 : 0);
		// Calculate the amount of zero's that need to be prefixed
		int requiredPrefixLength = (minimumLength - length);

		// Is the required amount of zero's > 0? If not, return current string.
		if (requiredPrefixLength < 1) return (isNegative ? "-" : "") + strNum;

		// Create StringBuilder
		StringBuilder builder = new StringBuilder();
		// If the number was negative, append a minus (-).
		if (isNegative) builder.append('-');
		// Append the required amount of zero's
		for (int i = 0; i < requiredPrefixLength; i++) {
			builder.append('0');
		}

		// Append number itself
		builder.append(strNum);

		// Return String
		return builder.toString();
	}

	/**
	 * Returns the extension extracted from the given file name.
	 * 
	 * @param fileName
	 *            The name of the file (with extension)
	 * @return The extension of the file
	 */
	public static String getFileExtension(String fileName) {
		if (fileName == null || fileName.equals("")) return "";

		String[] fileArray = fileName.split("\\.");
		if (fileArray.length < 2) {
			return "";
		} else {
			return fileArray[fileArray.length - 1];
		}
	}

	public static String getFileExtension(File file) {
		return getFileExtension(file.getName());
	}

	public static <T> Predicate<T> not(Predicate<T> predicate) {
	    return predicate.negate();
    }

    public static Rectangle fitACompletelyInB(Dimension a, Dimension b) {
		double scale = Math.min(b.getWidth() / a.getWidth(), b.getHeight() / a.getHeight());
		int width = (int) Math.round(a.getWidth() * scale);
		int height = (int) Math.round(a.getHeight() * scale);
		int x = (int) Math.round((b.getWidth() - width) / 2);
		int y = (int) Math.round((b.getHeight() - height) / 2);
		return new Rectangle(x, y, width, height);
	}
}

package org.acisoft.core;

import java.io.InputStream;
import java.util.Optional;

public class PlayerWindowOptions {
    private String title;
    private InputStream icon;

    public PlayerWindowOptions(String title, InputStream icon) {
        this.title = title;
        this.icon = icon;
    }

    public PlayerWindowOptions(String title) {
        this(title, null);
    }

    public String getTitle() {
        return title;
    }

    public Optional<InputStream> getIcon() {
        return Optional.ofNullable(icon);
    }
}

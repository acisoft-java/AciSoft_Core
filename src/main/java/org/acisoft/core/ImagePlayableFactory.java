package org.acisoft.core;

import org.acisoft.core.playable.customplayable.Image;
import org.acisoft.exception.AciSoftException;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static java.util.Arrays.asList;
import static org.acisoft.core.Utils.getFileExtension;

public class ImagePlayableFactory implements PlayableFactory {
    private static final List<String> supportedFileExtensions = asList(ImageIO.getReaderFileSuffixes());

    @Override
    public boolean accepts(File file) {
        return supportedFileExtensions.contains(getFileExtension(file));
    }

    @Override
    public Image createFrom(File file) {
        try {
            return Image.load(file);
        } catch (IOException e) {
            throw new CouldNotLoadImageException(file, e);
        }
    }

    @Override
    public void dispose() {
        // This factory has nothing to dispose
    }

    public static class CouldNotLoadImageException extends AciSoftException {
        public CouldNotLoadImageException(File file, Throwable cause) {
            super("Could not load " + file + " as image.", cause);
        }
    }
}

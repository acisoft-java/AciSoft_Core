package org.acisoft.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.NoArgsConstructor;
import org.acisoft.exception.VlcNotFoundException;
import org.acisoft.exception.IllegalStateException;
import org.acisoft.exception.IncompatibleBitArchitectureException;
import org.acisoft.libconnect.vlcj.VlcjMediaPlayerFactory;

import com.sun.jna.NativeLibrary;

import org.acisoft.libconnect.vlcj.finder.VlcFinder;
import org.acisoft.libconnect.vlcj.finder.VlcFinderFactory;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.caprica.vlcj.runtime.RuntimeUtil;

import static java.util.Arrays.asList;

/**
 * <p>Default implementation for the AciSoft Core using vlcj as playback library.
 * From here, everything in the core should be reachable.
 *
 * <p>Once the core is stopped it cannot be rebooted and must be disposed.
 *
 * @author regapictures
 */
@NoArgsConstructor
public final class VlcjAciSoftCore extends AciSoftCore {
    private static final Logger LOGGER = LoggerFactory.getLogger(VlcjAciSoftCore.class);
    private PlayerWindowOptions playerWindowOptions;

    public VlcjAciSoftCore(PlayerWindowOptions playerWindowOptions) {
        this.playerWindowOptions = playerWindowOptions;
    }

    public PlayerWindowOptions getPlayerWindowOptions() {
        return Optional.ofNullable(playerWindowOptions).orElse(new PlayerWindowOptions("AciSoft Player"));
    }

    @Override
    public void boot() throws IncompatibleBitArchitectureException, VlcNotFoundException {
        require64BitArchitecture();
        setupNativeVLClibrary();
        setupScheduler();
        setupMediaPlayerFactory();

        // Create a new PlayableFactory
        this.setPlayableFactory(new DelegatingPlayableFactory());

        // Create a new AciPlayer
        this.setPlayer(new AciPlayer(this.getPlayerFactory(), this.getPlayerWindowOptions()));
    }

    @Override
    public void stop() {
        Scheduler.stop();

        player.stop();
        player.hideWindow();
        player.dispose();
        playerFactory.dispose();
        playableFactory.dispose();
        SWTResourceManager.dispose();
    }

    private void setupMediaPlayerFactory() {
        try {
            this.setPlayerFactory(new VlcjMediaPlayerFactory());
        } catch (IllegalStateException e) {
            LOGGER.warn("Could not set PlayerFactory. Has it already been set before?", e);
        } catch (RuntimeException e) {
            if (isVlcLibraryNotFoundMessage(e.getMessage())) {
                throw new VlcNotFoundException(e);
            }
        }
    }

    private boolean isVlcLibraryNotFoundMessage(String errMessage) {
        return errMessage.contains("Failed to load the native library") && errMessage.contains("vlc");
    }

    private void setupScheduler() {
        try {
            new Scheduler(50);
        } catch (IllegalStateException e) {
            // Ignore
        }
    }

    private void setupNativeVLClibrary() throws VlcNotFoundException {
        final VlcFinder vlcFinder = new VlcFinderFactory().searchSpecificDirectoriesAndThenVlcjFinder(getVlcSearchDirectories());
        final Optional<File> vlcDirectory = vlcFinder.findVlc();

        if (vlcDirectory.isPresent()) {
            NativeLibrary.addSearchPath(RuntimeUtil.getLibVlcLibraryName(), vlcDirectory.get().getPath());
        } else {
            throw new VlcNotFoundException();
        }
    }

    private List<String> getVlcSearchDirectories() {
        List<String> vlcSearchDirectories = new ArrayList<>();
        vlcSearchDirectories.addAll(asList("vlc", getJarFile().getParent() + "\\vlc", "..\\AciSoft_Core\\vlc"));
        vlcSearchDirectories.addAll(generateProgramFileDirectoryPaths());
        return vlcSearchDirectories;
    }

    private List<String> generateProgramFileDirectoryPaths() {
        final File[] roots = File.listRoots();
        List<String> generatedPaths = new ArrayList<>();
        for (File root : roots) {
            String rootPath = root.getAbsolutePath();
            generatedPaths.add(rootPath + "\\Program Files (x86)\\VideoLAN\\VLC");
            generatedPaths.add(rootPath + "\\Program Files\\VideoLAN\\VLC");
        }

        return generatedPaths;
    }

    private void require64BitArchitecture() throws IncompatibleBitArchitectureException {
        // Test if JVM is 64-bit (required)
        String bitArch = System.getProperty("os.arch");
        if (!bitArch.contains("64")) {
            throw new IncompatibleBitArchitectureException(
                    "AciSoft requires a 64-bit Java Virtual Machine to run. Current architecture: " + bitArch);
        }
    }
}

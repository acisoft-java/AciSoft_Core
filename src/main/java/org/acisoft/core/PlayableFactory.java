package org.acisoft.core;

import org.acisoft.core.playable.Playable;

import java.io.File;

public interface PlayableFactory {
	boolean accepts(File file);
	Playable createFrom(File file);

	void dispose();
}

package org.acisoft.core.gui;

import org.eclipse.swt.widgets.Display;

public class WindowRunner {
	private Window window;

	public WindowRunner(Window w) {
		if (w == null) throw new NullPointerException("Cannot create runner for a null pointer!");

		this.window = w;
	}

	/**
	 * Runs the {@link Window} in this {@link WindowRunner} and shows the
	 * window.
	 */
	public void run() {
		this.run(true);
	}

	/**
	 * Runs the {@link Window} in this {@link WindowRunner}.
	 * 
	 * @param visible
	 *            Whether the window should be visible (<code>true</code>) or
	 *            not (<code>false</code>)
	 */
	public void run(boolean visible) {
		if (visible) {
			this.window.open();
			// this.window.setVisible(visible);
		}
		this.window.layout();

		Display display = this.window.getDisplay();
		while (!this.window.isDisposed() && !this.window.mustExit()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
		window.dispose(); // TODO Fix deposing so no "already disposed" error occurs any more
	}

	/**
	 * Stops this runner by sending a signal to the window. Once stopped, this
	 * {@link WindowRunner} (and Window) must disposed and can't be reused.
	 */
	public void stop() {
		this.window.exit();
	}
}

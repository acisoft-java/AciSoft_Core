package org.acisoft.core.gui;

import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Window extends Shell {

	private boolean exit;
	private boolean opened;

	/**
	 * Create the shell.
	 * 
	 * @param display
	 */
	public Window(Display display, int style) {
		super(display, style);
		this.exit = false;
		this.opened = false;

		addShellListener(new ShellAdapter() {
			@Override
			public void shellClosed(ShellEvent e) {
				onClose(e);
			}
		});
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public boolean mustExit() {
		return this.exit;
	}

	public void exit() {
	    getDisplay().asyncExec(() -> {
	    	this.exit = true;
		});
	}

	protected void onClose(ShellEvent e) {
		this.exit = true;
	}

	@Override
	public void open() {
		super.open();
		this.opened = true;
	}

	@Override
	public void close() {
		super.close();
		this.opened = false;
	}

	public boolean isOpened() {
		return this.opened;
	}
}
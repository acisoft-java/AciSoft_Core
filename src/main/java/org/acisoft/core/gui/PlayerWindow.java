package org.acisoft.core.gui;

import lombok.Getter;
import org.acisoft.core.PlayerWindowOptions;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

import static java.util.Arrays.stream;

public class PlayerWindow extends Window {
    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerWindow.class);

    /**
     * Composite used as video screen.
     */
    private Composite screen;

    /**
     * Pointer pointing to the screen where this window is currently on.
     */
    @Getter
    private Monitor targetMonitor;

    public PlayerWindow(Display display, PlayerWindowOptions options) {
        super(display, SWT.NO_TRIM);
        this.createContents(options);
    }

    /**
     * Create contents of the window.
     * @param options
     */
    protected void createContents(PlayerWindowOptions options) {
        // Set title of this window
        this.setText(options.getTitle());
        // Set background of this window
        this.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
        // Set layout of this window
        GridLayout gridLayout = new GridLayout(1, false);
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        setLayout(gridLayout);

        // Create and configure Composite that will be the video screen
        this.screen = new Composite(this, SWT.EMBEDDED);
        this.screen.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
        this.screen.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        this.screen.setLayout(new FillLayout(SWT.HORIZONTAL));
        this.setIcon(options);
    }

    private void setIcon(PlayerWindowOptions options) {
        try {
            options.getIcon()
                    .map(stream -> new Image(this.getDisplay(), stream))
                    .ifPresent(this::setImage);
        } catch (Exception e) {
            LOGGER.info("Failed to display icon in player window.", e);
        }
    }

    public void setTargetMonitor(Monitor monitor) {
        if (!monitor.equals(targetMonitor)) {
            setBounds(monitor.getBounds());
            setMaximized(true);
            setFullScreen(true);
            targetMonitor = monitor;
        }
    }

    /**
     * Return the {@link Composite} that should be used as the video screen.
     * 
     * @return The {@link Composite} that should be used as the video screen
     */
    public Composite getVideoScreen() {
        return this.screen;
    }
}

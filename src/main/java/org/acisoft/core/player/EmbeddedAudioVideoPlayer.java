package org.acisoft.core.player;

import java.awt.*;

/**
 * Interface that represents the simplest embedded audio/video player unit. <br />
 * <br />
 * This interface should be implemented for usage with a specific playback
 * library (default: vlcj).
 * 
 * @author regapictures
 */
public interface EmbeddedAudioVideoPlayer extends Player {
	Canvas getCanvas();
}

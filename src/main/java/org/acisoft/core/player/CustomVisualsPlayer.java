package org.acisoft.core.player;

import lombok.AllArgsConstructor;
import org.acisoft.core.playable.customplayable.CustomVisual;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Optional;

@AllArgsConstructor
public class CustomVisualsPlayer {
    private PaintableCanvas canvas;

    public void show(CustomVisual customVisual) {
        canvas.showCustomVisual(customVisual);
    }

    public void clear() {
        canvas.clearCustomVisual();
    }

    public boolean isShowingCustomVisualRightNow() {
        return canvas.isCustomVisualShown();
    }

    public Optional<CustomVisual> getCurrentShownCustomVisual() {
        return canvas.getCurrentCustomVisual();
    }

    public static class PaintableCanvas extends Canvas {
        private CustomVisual customVisual;

        public void showCustomVisual(CustomVisual customVisual) {
            this.customVisual = customVisual;
            repaint();
        }

        public void clearCustomVisual() {
            this.customVisual = null;
            repaint();
        }

        public boolean isCustomVisualShown() {
            return customVisual != null;
        }

        public Optional<CustomVisual> getCurrentCustomVisual() {
            return Optional.ofNullable(customVisual);
        }

        @Override
        public void update(Graphics g) {
            paint(g);
        }

        @Override
        public void paint(Graphics graphics) {
            if (customVisual == null) {
                clear(graphics);
            } else {
                paintCustomVisual(graphics, customVisual);
            }
        }

        private void paintCustomVisual(Graphics graphics, CustomVisual customVisual) {
            Image buffer = paintOnBuffer(customVisual);
            // Buffer must be painted first before clearing to avoid flickering
            clear(graphics);
            graphics.drawImage(buffer, 0, 0, (img, infoflags, x, y, width, height) -> false);
        }

        private Image paintOnBuffer(CustomVisual customVisual) {
            Image buffer = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
            customVisual.drawOn(getSize(), (Graphics2D) buffer.getGraphics());
            return buffer;
        }

        private void clear(Graphics graphics) {
            super.paint(graphics);
        }
    }
}

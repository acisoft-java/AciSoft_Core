package org.acisoft.core.player;

import org.acisoft.core.PlayState;
import org.acisoft.core.Time;
import org.acisoft.core.playable.Playable;
import org.acisoft.core.playable.customplayable.CustomVisual;
import org.acisoft.exception.IllegalStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class CustomVisualsPlayerAdapter implements Player {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomVisualsPlayerAdapter.class);

    private CustomVisualsPlayer customVisualsPlayer;
    private CustomVisual loadedCustomVisual;
    private CustomVisual idleCustomVisual;

    public CustomVisualsPlayerAdapter(CustomVisualsPlayer customVisualsPlayer) {
        this.customVisualsPlayer = customVisualsPlayer;
    }

    @Override
    public void dispose() {
        // Nothing to dispose
    }

    @Override
    public void play() {
        if (hasLoadedPlayable()) {
            playUnchecked(loadedCustomVisual);
        }
    }

    @Override
    public void play(Playable playable) {
        if (this.supportsPlaybackOf(playable)) {
            playUnchecked(playable);
        } else {
            logPlaybackNotSupportedFor(playable);
        }
    }

    private void playUnchecked(Playable playable) {
        CustomVisual customVisual = (CustomVisual) playable;
        loadedCustomVisual = customVisual;
        customVisualsPlayer.show(customVisual);
    }

    @Override
    public boolean canPlay() {
        return loadedCustomVisual != null;
    }

    @Override
    public void pause() {
        // Currently, cannot pause a custom playable
    }

    @Override
    public void playPause() {
        // Currently, cannot pause a custom playable
    }

    @Override
    public boolean canPause() {
        // Currently, cannot pause a custom playable
        return false;
    }

    @Override
    public void stop() {
        if (idleCustomVisual == null) {
            customVisualsPlayer.clear();
        } else {
            customVisualsPlayer.show(idleCustomVisual);
        }
    }

    @Override
    public boolean canStop() {
        return customVisualsPlayer.isShowingCustomVisualRightNow();
    }

    @Override
    public void load(Playable playable) {
        if (this.supportsPlaybackOf(playable)) {
            loadedCustomVisual = (CustomVisual) playable;
        } else {
            logPlaybackNotSupportedFor(playable);
        }
    }

    @Override
    public Optional<Playable> getLoadedPlayable() {
        return Optional.ofNullable(loadedCustomVisual);
    }

    @Override
    public boolean hasLoadedPlayable() {
        return loadedCustomVisual != null;
    }

    @Override
    public boolean supportsPlaybackOf(Playable playable) {
        return playable instanceof CustomVisual;
    }

    @Override
    public PlayState getPlayState() throws IllegalStateException {
        return isPlaying() ? PlayState.PLAYING : PlayState.STOPPED;
    }

    @Override
    public boolean isPlaying() {
        return customVisualsPlayer.getCurrentShownCustomVisual()
                .filter(currentCustomVisual -> currentCustomVisual.equals(loadedCustomVisual))
                .isPresent();
    }

    @Override
    public boolean isPaused() {
        return false;
    }

    @Override
    public boolean isStopped() {
        return !isPlaying();
    }

    @Override
    public Time getTime() {
        return Time.ZERO;
    }

    @Override
    public void setTime(Time time) {
        // No time to set
    }

    @Override
    public int getVolume() {
        return 100;
    }

    @Override
    public void setVolume(int volume) {
        // No volume to set
    }

    public Optional<CustomVisual> getIdleCustomVisual() {
        return Optional.ofNullable(idleCustomVisual);
    }

    public void setIdleCustomVisual(CustomVisual customVisual) {
        idleCustomVisual = customVisual;
        if (isStopped()) {
            customVisualsPlayer.show(idleCustomVisual);
        }
    }

    public void removeIdleCustomVisual() {
        idleCustomVisual = null;
        if (isStopped()) {
            customVisualsPlayer.clear();
        }
    }

    private void logPlaybackNotSupportedFor(Playable playable) {
        LOGGER.warn("Cannot play {}, this player adapter only supports custom visuals", playable);
    }
}

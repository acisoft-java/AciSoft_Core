package org.acisoft.core.player;

import org.acisoft.core.PlayState;
import org.acisoft.core.PlayerWindowOptions;
import org.acisoft.core.Scheduler;
import org.acisoft.core.Time;
import org.acisoft.core.Updateable;
import org.acisoft.core.gui.PlayerWindow;
import org.acisoft.core.gui.WindowRunner;
import org.acisoft.core.playable.NOPPlayer;
import org.acisoft.core.playable.Playable;
import org.acisoft.core.playable.customplayable.CustomVisual;
import org.acisoft.exception.IllegalStateException;
import org.acisoft.exception.UnsupportedFileException;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import static java.util.Arrays.stream;
import static org.acisoft.core.Utils.not;

/**
 * <p>
 * Class to wrap around the {@link EmbeddedAudioVideoPlayer}.
 * </p>
 *
 * <p>
 * {@link EmbeddedAudioVideoPlayer} (implemented by any playback library) only cares
 * about the {@link Composite} it needs to render the video on, so this class
 * takes care of the window that holds that {@link Composite} by wrapping around
 * the {@link EmbeddedAudioVideoPlayer}.
 * </p>
 *
 * @author regapictures
 */
public class AciSoftEmbeddedMediaPlayer implements EmbeddedAudioVideoPlayer, Updateable {
    private static final Logger LOGGER = LoggerFactory.getLogger(AciSoftEmbeddedMediaPlayer.class);

    private Monitor availableTargetMonitor;
    private WindowRunner windowRunner;
    private PlayerWindow window;
    private CustomVisualsPlayer.PaintableCanvas canvas;

    private List<Player> players;
    private Player activePlayer = new NOPPlayer();
    private CustomVisualsPlayerAdapter customVisualsPlayer;

    /*
     * We are using this local flag because we can only do window.getVisible() using the
     * PlayerWindow's thread, which is a pain to do.
     */
    private boolean userSetWindowToVisible;

    /**
     * <p>
     * Creates a new {@link AciSoftEmbeddedMediaPlayer}, with the given
     * {@link PlayerFactory} to create new players.
     * </p>
     *
     * @param factory             The {@link PlayerFactory} to use for creating players
     * @param isDaemon            Whether the {@link PlayerWindow}'s thread should be a daemon
     *                            (<code>true</code>) or not (<code>false</code>).
     * @param playerWindowOptions Options for the player window.
     */
    public AciSoftEmbeddedMediaPlayer(PlayerFactory factory, boolean isDaemon, PlayerWindowOptions playerWindowOptions) {
        players = new ArrayList<>();

        createAndStartPlayerWindowThread(factory, isDaemon, playerWindowOptions);

        // Sleep some time, so the new thread can assign the fields that are
        // also used by this object.
        while (players.isEmpty()) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                break;
            }
        }

        createCustomVisualsPlayer();

        Scheduler.register(this);
    }

    public void showWindow() {
        setWindowVisible(true);
    }

    public void hideWindow() {
        setWindowVisible(false);
    }

    public boolean isWindowVisible() {
        return this.userSetWindowToVisible;
    }

    private void setWindowVisible(boolean visible) {
        window.getDisplay().asyncExec(() -> {
            window.setVisible(visible);
            window.forceActive();

            userSetWindowToVisible = visible;
        });
    }

    public Optional<Monitor> getAvailableTargetMonitorForWindow() {
        return Optional.ofNullable(availableTargetMonitor);
    }

    @Override
    public void onUpdate() {
        window.getDisplay().syncExec(this::updatePosition);
    }

    @Override
    public void dispose() {
        window.exit();
        forEachPlayerSafelyDo(Player::dispose);
    }

    @Override
    public void play() {
        activePlayer.play();
    }

    @Override
    public void play(Playable playable) throws UnsupportedFileException {
        activePlayer = getPlayerFor(playable);
        stopNonActivePlayers();
        activePlayer.play(playable);
    }

    @Override
    public boolean canPlay() {
        return activePlayer.canPlay();
    }

    @Override
    public void pause() {
        activePlayer.pause();
    }

    @Override
    public void playPause() {
        activePlayer.playPause();
    }

    @Override
    public boolean canPause() {
        return activePlayer.canPause();
    }

    @Override
    public void stop() {
        activePlayer.stop();
    }

    @Override
    public boolean canStop() {
        return activePlayer.canStop();
    }

    @Override
    public void load(Playable playable) throws UnsupportedFileException {
        activePlayer = getPlayerFor(playable);
        stopNonActivePlayers();
        activePlayer.load(playable);
    }

    @Override
    public Optional<Playable> getLoadedPlayable() {
        return activePlayer.getLoadedPlayable();
    }

    @Override
    public boolean hasLoadedPlayable() {
        return activePlayer.hasLoadedPlayable();
    }

    @Override
    public boolean supportsPlaybackOf(Playable playable) {
        return findPlayerFor(playable).isPresent();
    }

    @Override
    public PlayState getPlayState() throws IllegalStateException {
        return activePlayer.getPlayState();
    }

    @Override
    public boolean isPlaying() {
        return activePlayer.isPlaying();
    }

    @Override
    public boolean isPaused() {
        return activePlayer.isPaused();
    }

    @Override
    public boolean isStopped() {
        return activePlayer.isStopped();
    }

    @Override
    public Time getTime() {
        return activePlayer.getTime();
    }

    @Override
    public void setTime(Time time) {
        activePlayer.setTime(time);
    }

    @Override
    public int getVolume() {
        return activePlayer.getVolume();
    }

    @Override
    public void setVolume(int volume) {
        activePlayer.setVolume(volume);
    }

    @Override
    public Canvas getCanvas() {
        return canvas;
    }

    public Optional<CustomVisual> getIdleCustomVisual() {
        return customVisualsPlayer.getIdleCustomVisual();
    }

    public void setIdleCustomVisual(CustomVisual customVisual) {
        customVisualsPlayer.setIdleCustomVisual(customVisual);
    }

    public void removeIdleCustomVisual() {
        customVisualsPlayer.removeIdleCustomVisual();
    }

    private Thread createAndStartPlayerWindowThread(PlayerFactory factory, boolean isDaemon, PlayerWindowOptions options) {
        Thread thread = new Thread(() -> {
            Display display = new Display();

            window = new PlayerWindow(display, options);
            windowRunner = new WindowRunner(window);
            canvas = createSwtAwtBridge(window.getVideoScreen());
            players.add(factory.newEmbeddedPlayer(canvas));

            // If this changes to true, the variable
            // AciSoftEmbeddedMediaPlayer.userSetWindowToVisible also must be initialized to true.
            windowRunner.run(false);
        }, "PlayerWindow");
        thread.setDaemon(isDaemon);
        thread.start();
        return thread;
    }

    private void createCustomVisualsPlayer() {
        customVisualsPlayer = new CustomVisualsPlayerAdapter(new CustomVisualsPlayer(canvas));
        players.add(customVisualsPlayer);
    }

    private CustomVisualsPlayer.PaintableCanvas createSwtAwtBridge(Composite composite) {
        CustomVisualsPlayer.PaintableCanvas jCanvas = new CustomVisualsPlayer.PaintableCanvas();
        jCanvas.setBackground(Color.BLACK);

        // Todo Issue #1: Add Panel as root for all components to fix mouse events not working (see docs of SWT_AWT.new_Frame)
        Frame jFrame = SWT_AWT.new_Frame(composite);
        jFrame.setBackground(Color.BLACK);
        jFrame.add(jCanvas);

        return jCanvas;
    }

    /**
     * <p>Repositions this window to the first non primary monitor, if not already on there.
     * <p>If there is only one or zero monitors, the PlayerWindow will be hidden. Once another monitor gets connected
     * again, the PlayerWindow will not automatically appear again after being hidden.
     */
    private void updatePosition() {
        Optional<Monitor> nonPrimaryMonitor = getFirstNonPrimaryMonitorAvailable();
        if (nonPrimaryMonitor.isPresent()) {
            Monitor targetMonitor = nonPrimaryMonitor.get();
            window.setTargetMonitor(targetMonitor);
            window.setVisible(userSetWindowToVisible);
            this.availableTargetMonitor = targetMonitor;
        } else {
            window.setVisible(false);
            this.availableTargetMonitor = null;
        }
    }

    private Optional<Monitor> getFirstNonPrimaryMonitorAvailable() {
        Monitor primaryMonitor = window.getDisplay().getPrimaryMonitor();
        return stream(window.getDisplay().getMonitors())
                .filter(not(primaryMonitor::equals))
                .findFirst();
    }

    private void forEachPlayerSafelyDo(Consumer<Player> playerConsumer) {
        players.forEach(player -> {
            try {
                playerConsumer.accept(player);
            } catch (Exception e) {
                LOGGER.error("Error for player {} when looping over players", player.getClass(), e);
            }
        });
    }

    private Optional<Player> findPlayerFor(Playable playable) {
        Optional<Player> optionalPlayer = players.stream()
                .filter(player -> player.supportsPlaybackOf(playable))
                .findAny();
        LOGGER.debug("Player search result for playable {}: {}", playable, optionalPlayer);
        return optionalPlayer;
    }

    private Player getPlayerFor(Playable playable) throws UnsupportedFileException {
        return findPlayerFor(playable).orElseThrow(UnsupportedFileException::new);
    }

    private void stopNonActivePlayers() {
        players.stream()
                .filter(not(activePlayer::equals))
                .forEach(Player::stop);
    }
}

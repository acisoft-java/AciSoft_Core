package org.acisoft.core.player;

import java.awt.*;

public interface PlayerFactory {
	/**
	 * Creates a new {@link EmbeddedAudioVideoPlayer}.
	 * 
	 * @param screen
	 * @return The new {@link EmbeddedAudioVideoPlayer}
	 */
	EmbeddedAudioVideoPlayer newEmbeddedPlayer(Canvas screen);

	/**
	 * Creates a new headless {@link Player}.
	 * 
	 * @return The new headless {@link Player}
	 */
	Player newHeadlessPlayer();

	/**
	 * Free all resources used by this {@link PlayerFactory}.
	 */
	void dispose();
}

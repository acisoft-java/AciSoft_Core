package org.acisoft.core.player;

import org.acisoft.core.PlayState;
import org.acisoft.core.Time;
import org.acisoft.core.playable.Playable;
import org.acisoft.exception.IllegalStateException;

import java.util.Optional;

/**
 * Interface that represents the simplest player unit. <br />
 * <br />
 * This interface should be implemented for usage with a specific playback
 * library (default: vlcj).
 * 
 * @author regapictures
 */
public interface Player {
	/**
	 * Free all resources used by this {@link Player}.
	 */
	void dispose();

	/**
	 * Start playing the currently loaded {@link Playable}, if any.
	 */
	void play();

	/**
	 * Start playing the given {@link Playable}.
	 * 
	 * @param playable
	 *            The {@link Playable} to play
	 */
	void play(Playable playable);

	/**
	 * Checks if calling {@link #play()} will succeed, and playback will start.
	 * 
	 * @return If calling {@link #play()} will succeed
	 */
	boolean canPlay();

	/**
	 * Pause the playback.
	 */
	void pause();

	/**
	 * Toggle play/pause.
	 */
	void playPause();

	/**
	 * Checks if calling {@link #pause()} will succeed, and playback can be
	 * paused.
	 * 
	 * @return If calling {@link #pause()} will succeed
	 */
	boolean canPause();

	/**
	 * Stops the playback, and resets {@link Player} time to zero.
	 */
	void stop();

	/**
	 * Checks if calling {@link #stop()} will succeed, and playback can be
	 * stopped.
	 * 
	 * @return If calling {@link #stop()} will succeed
	 */
	boolean canStop();

	/**
	 * Loads the given {@link Playable}, but doesn't start playing it.
	 * 
	 * @param playable
	 *            The {@link Playable} to load
	 */
	void load(Playable playable);

	/**
	 * Returns the current loaded {@link Playable}, if any.
	 * 
	 * @return The current loaded {@link Playable}, if any
	 */
	Optional<Playable> getLoadedPlayable();

	/**
	 * Returns whether anything is currently loaded.
	 * 
	 * @return Whether anything is currently loaded
	 */
	boolean hasLoadedPlayable();

	/**
	 * Returns whether this {@link Player} supports playback for the given {@link Playable}.
	 * @param playable The Playable of which the caller wants to know if it is supported
	 * @return Whether this {@link Player} supports playback for the given {@link Playable}
	 */
	boolean supportsPlaybackOf(Playable playable);

	/**
	 * Returns the current {@link PlayState}.
	 * 
	 * @return The current {@link PlayState}
	 * @throws IllegalStateException
	 *             When the underlying playback library is in an unknown state
	 *             (that is not programmatically expected to occur). This could
	 *             occur for example when a library update introduces a new
	 *             (unknown) play state.
	 */
	PlayState getPlayState() throws IllegalStateException;

	/**
	 * Returns whether this {@link Player} is playing.
	 * 
	 * @return Whether this {@link Player} is playing
	 */
	boolean isPlaying();

	/**
	 * Returns whether this {@link Player} is paused.
	 * 
	 * @return Whether this {@link Player} is paused
	 */
	boolean isPaused();

	/**
	 * Returns whether this {@link Player} is stopped or idle.
	 * 
	 * @return Whether this {@link Player} is stopped or idle
	 */
	boolean isStopped();

	/**
	 * Returns the current playback {@link Time}.
	 * 
	 * @return The current playback {@link Time}
	 */
	Time getTime();

	/**
	 * Sets the current playback {@link Time}.
	 * 
	 * @param time
	 *            The playback {@link Time} to set
	 */
	void setTime(Time time);

	/**
	 * Returns the current set play back volume.
	 * 
	 * @return The current set play back volume
	 */
	int getVolume();

	/**
	 * Sets the play back volume.
	 * 
	 * @param volume
	 *            The play back volume to set
	 */
	void setVolume(int volume);
}

package org.acisoft.core;

import java.util.ArrayList;

import org.acisoft.core.playable.Playable;

public class Playlist {
	private ArrayList<Playable> playlist;

	public Playlist() {
		this.playlist = new ArrayList<>();
	}

	public void add(Playable m) {
		this.playlist.add(m);
	}

	public void remove(Playable m) {
		this.playlist.remove(m);
	}

	public void clear() {
		this.playlist.clear();
	}

	public int getLength() {
		return this.playlist.size();
	}

	public Playable getAtIndex(int i) {
		if (i < 0 || i >= this.playlist.size()) return null;
		return this.playlist.get(i);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Playlist)) return false;

		Playlist p = (Playlist) o;
		return this.playlist.equals(p.playlist);
	}
}

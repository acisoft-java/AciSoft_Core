package org.acisoft.core;

import org.acisoft.core.playable.Playable;
import org.acisoft.exception.UnsupportedFileException;
import org.acisoft.libconnect.vlcj.VlcjPlayableFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class DelegatingPlayableFactory implements PlayableFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(DelegatingPlayableFactory.class);

    private Set<PlayableFactory> factories = new HashSet<>();

    public DelegatingPlayableFactory() {
        factories.add(new ImagePlayableFactory());
        factories.add(new VlcjPlayableFactory());
    }

    @Override
    public boolean accepts(File file) {
        return factories.stream()
                .anyMatch(playableFactory -> playableFactory.accepts(file));
    }

    @Override
    public Playable createFrom(File file) {
        return factories.stream()
                .filter(playableFactory -> playableFactory.accepts(file))
                .findAny()
                .map(playableFactory -> playableFactory.createFrom(file))
                .orElseThrow(() -> new UnsupportedFileException("No PlayableFactory found capable of reading " + file));
    }

    @Override
    public void dispose() {
        for (PlayableFactory factory : factories) {
            attemptDisposeOf(factory);
        }
    }

    private void attemptDisposeOf(PlayableFactory factory) {
        try {
            factory.dispose();
        } catch (Exception e) {
            LOGGER.error("Failed to dispose PlayableFactory \"{}\"", factory.getClass(), e);
        }
    }
}

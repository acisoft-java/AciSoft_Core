package org.acisoft.core;

import java.time.Duration;

/**
 * Class to store a time value. <br />
 * <br />
 * Note that whenever <i>setting</i> a value (constructor or setter) with an
 * overflow (for example: minutes = 62 > 59) will cause the next larger unit to
 * be increased (for example: hours += 1) and the value itself getting
 * recalculated to not overflow (for example: minutes = 2).
 * 
 * @author regapictures
 *
 */
public class Time {
	public static final Time ZERO = new Time(0, 0, 0, 0);

	private long hours;
	private int minutes;
	private int seconds;
	private int milliseconds;

	/**
	 * Creates a new {@link Time} object with the hours, minutes, seconds and
	 * milliseconds set at zero.
	 */
	public Time() {
		this(0, 0, 0, 0);
	}

	public Time(Duration duration) {

	}

	/**
	 * Creates a new {@link Time} object with the given minutes, seconds and
	 * milliseconds. Hours will be set to zero. <br />
	 * <br />
	 * Overflows will cause a recalculation.
	 * 
	 * @param minutes
	 *            Minute value
	 * @param seconds
	 *            Second value
	 * @param milliseconds
	 *            Millisecond value
	 */
	public Time(int minutes, int seconds, int milliseconds) {
		this(0, minutes, seconds, milliseconds);
	}

	/**
	 * Creates a new {@link Time} object with the given hours, minutes and
	 * seconds. Milliseconds will be set to zero. <br />
	 * <br />
	 * Overflows will cause a recalculation.
	 * 
	 * @param hours
	 *            Hour value
	 * @param minutes
	 *            Minute value
	 * @param seconds
	 *            Second value
	 */
	public Time(long hours, int minutes, int seconds) {
		this(hours, minutes, seconds, 0);
	}

	/**
	 * Creates a new {@link Time} object with the given hours, minutes, seconds
	 * and milliseconds. <br />
	 * <br />
	 * Overflows will cause a recalculation.
	 * 
	 * @param hours
	 *            Hour value
	 * @param minutes
	 *            Minute value
	 * @param seconds
	 *            Second value
	 * @param milliseconds
	 *            Millisecond value
	 */
	public Time(long hours, int minutes, int seconds, int milliseconds) {
		this.setHours(hours);
		this.setMinutes(minutes);
		this.setSeconds(seconds);
		this.setMilliseconds(milliseconds);
	}

	/**
	 * Creates a new {@link Time} object from the given milliseconds.
	 * 
	 * @param milliseconds
	 *            The milliseconds to create a {@link Time} object of
	 * @return The new {@link Time} object
	 */
	public static Time fromMilliseconds(int milliseconds) {
		return new Time((int) 0, 0, milliseconds);
	}

	/**
	 * Converts the current {@link Time} to an amount of milliseconds.
	 * 
	 * @return The amount of milliseconds
	 */
	public int toMilliseconds() {
		return (((int) hours * 60 * 60 * 1000) + (minutes * 60 * 1000) + (seconds * 1000) + milliseconds);
	}

	/**
	 * Gets the hour value.
	 * 
	 * @return The current hour value
	 */
	public long getHours() {
		return hours;
	}

	/**
	 * Sets the hour value.
	 * 
	 * @param hours
	 *            The hour value to set
	 */
	public void setHours(long hours) {
		this.hours = hours;
	}

	/**
	 * Gets the minute value.
	 * 
	 * @return The current minute value
	 */
	public int getMinutes() {
		return minutes;
	}

	/**
	 * Sets the minute value. <br />
	 * <br />
	 * An overflow will cause a recalculation.
	 * 
	 * @param minutes
	 *            The minute value to set
	 */
	public void setMinutes(int minutes) {
		if (minutes >= 60) {
			int extra = minutes / 60;
			minutes %= 60;

			this.setHours(this.getHours() + extra);
		}

		this.minutes = minutes;
	}

	/**
	 * Gets the second value.
	 * 
	 * @return The current second value
	 */
	public int getSeconds() {
		return seconds;
	}

	/**
	 * Sets the second value. <br />
	 * <br />
	 * An overflow will cause a recalculation.
	 * 
	 * @param seconds
	 *            The second value to set
	 */
	public void setSeconds(int seconds) {
		if (seconds >= 60) {
			int extra = seconds / 60;
			seconds %= 60;

			this.setMinutes(this.getMinutes() + extra);
		}

		this.seconds = seconds;
	}

	/**
	 * Gets the millisecond value.
	 * 
	 * @return The current millisecond value
	 */
	public int getMilliseconds() {
		return milliseconds;
	}

	/**
	 * Sets the millisecond value. <br />
	 * <br />
	 * An overflow will cause a recalculation.
	 * 
	 * @param milliseconds
	 *            The millisecond value to set
	 */
	public void setMilliseconds(int milliseconds) {
		if (milliseconds >= 1000) {
			int extra = milliseconds / 1000;
			milliseconds %= 1000;

			this.setSeconds(this.getSeconds() + extra);
		}

		this.milliseconds = milliseconds;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Time)) return false;

		Time t = (Time) o;
		return this.milliseconds == t.milliseconds && this.seconds == t.seconds && this.minutes == t.minutes
				&& this.hours == t.hours;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();

		String hours = Utils.zeroPrefixNumber(this.getHours(), 2, false);
		String minutes = Utils.zeroPrefixNumber(this.getMinutes(), 2, false);
		String seconds = Utils.zeroPrefixNumber(this.getSeconds(), 2, false);
		String milliseconds = Utils.zeroPrefixNumber(this.getMilliseconds(), 3, false);

		str.append(hours);
		str.append(":");
		str.append(minutes);
		str.append(":");
		str.append(seconds);
		str.append(",");
		str.append(milliseconds);

		return str.toString();
	}
}

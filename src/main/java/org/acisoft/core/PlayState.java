package org.acisoft.core;

/**
 * Enum indicating a play status.
 * 
 * @author regapictures
 *
 */
public enum PlayState {
	/**
	 * When the current state is unknown
	 */
	UNKNOWN,

	/**
	 * When nothing is loaded and playing
	 */
	IDLE,

	/**
	 * When something is loaded but not playing
	 */
	STOPPED,

	/**
	 * When the playback is paused
	 */
	PAUSED,

	/**
	 * When playing
	 */
	PLAYING;

	@Override
	public String toString() {
		switch (this) {
			case IDLE:
				return "Idle";
			case PLAYING:
				return "Playing";
			case PAUSED:
				return "Paused";
			case STOPPED:
				return "Stopped";
			default:
				return "Unknown";
		}
	}
}

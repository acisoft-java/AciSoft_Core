package org.acisoft.core;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.acisoft.exception.IllegalStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Class that runs registered tasks at a fixed rate.
 * </p>
 * 
 * <p>
 * Other classes can use of this functionality by implementing
 * {@link Updateable} and register in the scheduler by using the
 * {@link #register(Updateable)} static method.
 * </p>
 * 
 * <p>
 * <b>NOTE:</b> There can only be one instance of this class. A reference to
 * that instance is also stored internally. You do not need that instance to
 * make use of the functionality of this class, because this is done on a static
 * way.
 * </p>
 * 
 * @author regapictures
 *
 */
public class Scheduler {
	private static Scheduler instance;

    private long delay;
    private ConcurrentMap<Updateable, ScheduledFuture> tasks;
    private ScheduledExecutorService executor;

    /**
	 * <p>
	 * Create a new Scheduler that runs the {@link Updateable}s every &lt;
	 * <code>delay</code>&gt; milliseconds.
	 * </p>
	 * 
	 * <p>
	 * <b>NOTE:</b> There can only exist one instance. If an instance already
	 * exist, an {@link IllegalArgumentException} is thrown.
	 * </p>
	 * 
	 * @param delay
	 *            The delay in milliseconds
	 * @throws IllegalArgumentException
	 *             When an instance of this class already exists
	 */
	public Scheduler(long delay) throws IllegalStateException {
        if (instance != null) throw new IllegalStateException(
				"An instance of the Scheduler already exist. There can only be one instance.");

        this.delay = delay;
        this.tasks = new ConcurrentHashMap<>();
        this.executor = Executors.newSingleThreadScheduledExecutor();
		instance = this;
	}

	/**
	 * @see #register(Updateable)
	 */
	private void _register(Updateable updater) {
        tasks.put(updater, executor.scheduleAtFixedRate(updater::onUpdate, delay, delay, TimeUnit.MILLISECONDS));
    }

	/**
	 * @see #unregister(Updateable)
	 */
	private void _unregister(Updateable updater) {
        tasks.remove(updater).cancel(false);
	}

	/**
	 * @see #stop()
	 */
	private void _stop() {
        executor.shutdown();
	}

	/**
	 * Register the specified {@link Updateable} to be run by the Scheduler.
	 * 
	 * @param updater
	 *            The {@link Updateable} to register
	 * @return If the {@link Updateable} was successfully added to the
	 *         subscription list
	 */
	public static void register(Updateable updater) {
		instance._register(updater);
	}

	/**
	 * Unregister the specified {@link Updateable} in the Scheduler, if it
	 * exists.
	 * 
	 * @param updater
	 *            The {@link Updateable} to unregister
	 * @return If the {@link Updateable} was successfully removed from the
	 *         subscription list (<code>false</code> could also indicate it
	 *         never was in the list)
	 */
	public static void unregister(Updateable updater) {
		if (instance != null) {
			instance._unregister(updater);
		}
	}

	/**
	 * Stops this {@link Scheduler} and perform cleanup. After calling this
	 * method, no tasks will be executed any more.
	 */
	public static void stop() {
		if (instance != null) {
			instance._stop();
		}
	}
}

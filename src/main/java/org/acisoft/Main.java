package org.acisoft;

import lombok.Getter;
import org.acisoft.core.AciPlayer;
import org.acisoft.core.AciSoftCore;
import org.acisoft.core.VlcjAciSoftCore;
import org.acisoft.exception.IncompatibleBitArchitectureException;
import org.acisoft.exception.VlcNotFoundException;

import javax.swing.*;

public final class Main {

    @Getter
	private AciSoftCore aciSoftCore;

	public Main() {
		this.aciSoftCore = new VlcjAciSoftCore();
	}

	public static void main(String[] args) {
		/*
		 * This main method shows how to use the AciSoftCore. This 'Main' class
		 * is not required for a working core, and is just here for
		 * demonstration purposes.
		 */
		Main m = new Main();
		m.boot();
		m.test();
	}

	public void boot() {
		try {
			this.aciSoftCore.boot();
		} catch (VlcNotFoundException e) {
			// This exception will be thrown if the vlc cannot be found
			e.printStackTrace();

			JOptionPane.showMessageDialog(null,
					"The program cannot find the required vlc library.\n\nThe program will exit.",
					"Cannot find libraries", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		} catch (IncompatibleBitArchitectureException e) {
			e.printStackTrace();

			JOptionPane
					.showMessageDialog(null,
							"The program must be run with 64-bit Java, but was started with " + e.getDetectedBitSize()
									+ "-bit Java.\n\nThe program will exit.",
							"64-bit required", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}

	public void test() {
		AciPlayer player = this.aciSoftCore.getPlayer();
		player.showWindow();

		// Do other desired stuff
	}
}

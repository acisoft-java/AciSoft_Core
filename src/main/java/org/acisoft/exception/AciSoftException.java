package org.acisoft.exception;

public class AciSoftException extends RuntimeException {

	private static final long serialVersionUID = 2202488342773404793L;

	public AciSoftException() {
		super();
	}

	public AciSoftException(String message) {
		super(message);
	}

	public AciSoftException(Throwable cause) {
		super(cause);
	}

	public AciSoftException(String message, Throwable cause) {
		super(message, cause);
	}

	public AciSoftException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}

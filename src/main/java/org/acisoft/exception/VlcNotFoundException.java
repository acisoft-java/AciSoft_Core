package org.acisoft.exception;

public class VlcNotFoundException extends AciSoftToUserDisplayableException {
	public VlcNotFoundException() {
		super("Failed to find required dependency VLC");
	}

    public VlcNotFoundException(RuntimeException e) {
        super(e);
    }

    @Override
	public String getMessageForUser() {
		return "Failed to find VLC on this computer. Please install VLC, if not yet installed.";
	}
}

package org.acisoft.exception;

public abstract class AciSoftToUserDisplayableException extends AciSoftException {
    public AciSoftToUserDisplayableException(String message) {
        super(message);
    }

    public AciSoftToUserDisplayableException(Throwable cause) {
        super(cause);
    }

    public abstract String getMessageForUser();
}

package org.acisoft.exception;

public class IncompatibleBitArchitectureException extends AciSoftException {

	private static final long serialVersionUID = 6295686455383942183L;

	private String detectedBitArch;
	private int detectedBitSize;

	public IncompatibleBitArchitectureException() {
		super();

		this.processDetectedBitArch();
	}

	public IncompatibleBitArchitectureException(String message) {
		super(message);

		this.processDetectedBitArch();
	}

	public IncompatibleBitArchitectureException(Throwable cause) {
		super(cause);

		this.processDetectedBitArch();
	}

	public IncompatibleBitArchitectureException(String message, Throwable cause) {
		super(message, cause);

		this.processDetectedBitArch();
	}

	public IncompatibleBitArchitectureException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

		this.processDetectedBitArch();
	}

	private void processDetectedBitArch() {
		this.detectedBitArch = System.getProperty("os.arch");
		if (detectedBitArch.contains("86") || detectedBitArch.contains("32")) {
			this.detectedBitSize = 32;
		} else if (detectedBitArch.contains("64")) {
			this.detectedBitSize = 64;
		}
	}

	public String getDetectedBitArch() {
		return this.detectedBitArch;
	}

	public int getDetectedBitSize() {
		return this.detectedBitSize;
	}
}

package org.acisoft.exception;

public class IllegalStateException extends AciSoftException {

	private static final long serialVersionUID = -71520188570834416L;

	public IllegalStateException() {
		super();
	}

	public IllegalStateException(String message) {
		super(message);
	}

	public IllegalStateException(Throwable cause) {
		super(cause);
	}

	public IllegalStateException(String message, Throwable cause) {
		super(message, cause);
	}

	public IllegalStateException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}

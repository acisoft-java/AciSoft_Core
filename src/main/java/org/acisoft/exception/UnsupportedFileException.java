package org.acisoft.exception;

public class UnsupportedFileException extends AciSoftException {

	private static final long serialVersionUID = -7088659439763311720L;

	public UnsupportedFileException() {
		super();
	}

	public UnsupportedFileException(String message) {
		super(message);
	}

	public UnsupportedFileException(Throwable cause) {
		super(cause);
	}

	public UnsupportedFileException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnsupportedFileException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}

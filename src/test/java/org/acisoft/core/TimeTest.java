package org.acisoft.core;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TimeTest {
	
	private Time time;
	
	public static final long INIT_HOURS = 1;
	public static final int INIT_MINUTES = 30;
	public static final int INIT_SECONDS = 20;
	public static final int INIT_MILLISECONDS = 400;

	@Before
	public void setUp() throws Exception {
		this.time = new Time(
				INIT_HOURS,
				INIT_MINUTES,
				INIT_SECONDS,
				INIT_MILLISECONDS
			);
	}

	@After
	public void tearDown() throws Exception {
		this.time = null;
	}

	@Test
	public void testTimeContructor_MustRenderZeroTime() {
		this.time = new Time();
		assertEquals("Hours should be 0", 0, this.time.getHours());
		assertEquals("Minutes should be 0", 0, this.time.getMinutes());
		assertEquals("Seconds should be 0", 0, this.time.getSeconds());
		assertEquals("Milliseconds should be 0", 0, this.time.getMilliseconds());
	}
	
	@Test
	public void testTimeIntIntIntContructor_WithoutOverflow() {
		this.time = new Time(42, 29, 138);
		
		// Expected
		long expHours = 0;
		int expMinutes = 42;
		int expSeconds = 29;
		int expMilliseconds = 138;
		
		// Actual result
		long actHours = this.time.getHours();
		int actMinutes = this.time.getMinutes();
		int actSeconds = this.time.getSeconds();
		int actMilliseconds = this.time.getMilliseconds();
		
		// Assert
		assertEquals(expHours, actHours);
		assertEquals(expMinutes, actMinutes);
		assertEquals(expSeconds, actSeconds);
		assertEquals(expMilliseconds, actMilliseconds);
	}

	@Test
	public void testTimeIntIntIntContructor_WithOverflow_GetsRecalculated() {
		this.time = new Time(78, 195, 2673);
		
		// Expected
		long expHours = 1;
		int expMinutes = 21;
		int expSeconds = 17;
		int expMilliseconds = 673;
		
		// Actual result
		long actHours = this.time.getHours();
		int actMinutes = this.time.getMinutes();
		int actSeconds = this.time.getSeconds();
		int actMilliseconds = this.time.getMilliseconds();
		
		// Assert
		assertEquals(expHours, actHours);
		assertEquals(expMinutes, actMinutes);
		assertEquals(expSeconds, actSeconds);
		assertEquals(expMilliseconds, actMilliseconds);
	}
	
	@Test
	public void testTimeLongIntIntContructor_WithoutOverflow() {
		this.time = new Time((long) 3, 16, 45);
		
		// Expected
		long expHours = 3;
		int expMinutes = 16;
		int expSeconds = 45;
		int expMilliseconds = 0;
		
		// Actual result
		long actHours = this.time.getHours();
		int actMinutes = this.time.getMinutes();
		int actSeconds = this.time.getSeconds();
		int actMilliseconds = this.time.getMilliseconds();
		
		// Assert
		assertEquals(expHours, actHours);
		assertEquals(expMinutes, actMinutes);
		assertEquals(expSeconds, actSeconds);
		assertEquals(expMilliseconds, actMilliseconds);
	}

	@Test
	public void testTimeLongIntIntContructor_WithOverflow_GetsRecalculated() {
		this.time = new Time((long) 3, 78, 195);
		
		// Expected
		long expHours = 4;
		int expMinutes = 21;
		int expSeconds = 15;
		int expMilliseconds = 0;
		
		// Actual result
		long actHours = this.time.getHours();
		int actMinutes = this.time.getMinutes();
		int actSeconds = this.time.getSeconds();
		int actMilliseconds = this.time.getMilliseconds();
		
		// Assert
		assertEquals(expHours, actHours);
		assertEquals(expMinutes, actMinutes);
		assertEquals(expSeconds, actSeconds);
		assertEquals(expMilliseconds, actMilliseconds);
	}
	
	@Test
	public void testTimeLongIntIntIntContructor_WithoutOverflow() {
		this.time = new Time(3, 2, 14, 576);
		
		// Expected
		long expHours = 3;
		int expMinutes = 2;
		int expSeconds = 14;
		int expMilliseconds = 576;
		
		// Actual result
		long actHours = this.time.getHours();
		int actMinutes = this.time.getMinutes();
		int actSeconds = this.time.getSeconds();
		int actMilliseconds = this.time.getMilliseconds();
		
		// Assert
		assertEquals(expHours, actHours);
		assertEquals(expMinutes, actMinutes);
		assertEquals(expSeconds, actSeconds);
		assertEquals(expMilliseconds, actMilliseconds);
	}

	@Test
	public void testTimeLongIntIntIntContructor_WithOverflow_GetsRecalculated() {
		this.time = new Time(3, 78, 195, 2673);
		
		// Expected
		long expHours = 4;
		int expMinutes = 21;
		int expSeconds = 17;
		int expMilliseconds = 673;
		
		// Actual result
		long actHours = this.time.getHours();
		int actMinutes = this.time.getMinutes();
		int actSeconds = this.time.getSeconds();
		int actMilliseconds = this.time.getMilliseconds();
		
		// Assert
		assertEquals(expHours, actHours);
		assertEquals(expMinutes, actMinutes);
		assertEquals(expSeconds, actSeconds);
		assertEquals(expMilliseconds, actMilliseconds);
	}

	@Test
	public void testGetHours() {
		long exp = INIT_HOURS;
		long act = this.time.getHours();
		
		assertEquals(exp, act);
	}

	@Test
	public void testSetHours() {
		long exp = 46;
		this.time.setHours(exp);
		
		long act = this.time.getHours();
		
		assertEquals(exp, act);
	}

	@Test
	public void testGetMinutes() {
		int exp = INIT_MINUTES;
		int act = this.time.getMinutes();
		
		assertEquals(exp, act);
	}
	
	@Test
	public void testSetMinutes_WithoutOverflow() {
		int incr = 2;
		this.time.setMinutes(INIT_MINUTES + incr);
		
		int exp = INIT_MINUTES + incr;
		int act = this.time.getMinutes();
		
		assertEquals(exp, act);
	}

	@Test
	public void testSetMinutes_WithOverflow_GetsRecalculated() {
		int incr = 2;
		int overflow = 60;
		
		this.time.setMinutes(INIT_MINUTES + incr * overflow);
		
		int exp = INIT_MINUTES;
		int act = this.time.getMinutes();
		assertEquals(exp, act);
		
		long expH = INIT_HOURS + incr;
		long actH = this.time.getHours();
		assertEquals(expH, actH);
	}

	@Test
	public void testGetSeconds() {
		int exp = INIT_SECONDS;
		int act = this.time.getSeconds();
		
		assertEquals(exp, act);
	}
	
	@Test
	public void testSetSeconds_WithoutOverflow() {
		int incr = 2;
		this.time.setSeconds(INIT_SECONDS + incr);
		
		int exp = INIT_SECONDS + incr;
		int act = this.time.getSeconds();
		
		assertEquals(exp, act);
	}

	@Test
	public void testSetSeconds_WithOverflow_GetsRecalculated() {
		int incr = 2;
		int overflow = 60;
		
		this.time.setSeconds(INIT_SECONDS + incr * overflow);
		
		int exp = INIT_SECONDS;
		int act = this.time.getSeconds();
		assertEquals(exp, act);
		
		exp = INIT_MINUTES + incr;
		act = this.time.getMinutes();
		assertEquals(exp, act);
	}

	@Test
	public void testGetMilliseconds() {
		int exp = INIT_MILLISECONDS;
		int act = this.time.getMilliseconds();
		
		assertEquals(exp, act);
	}
	
	@Test
	public void testSetMilliseconds_WithoutOverflow() {
		int incr = 2;
		this.time.setMilliseconds(INIT_MILLISECONDS + incr);
		
		int exp = INIT_MILLISECONDS + incr;
		int act = this.time.getMilliseconds();
		
		assertEquals(exp, act);
	}

	@Test
	public void testSetMilliseconds_WithOverflow_GetsRecalculated() {
		int incr = 2;
		int overflow = 1000;
		
		this.time.setMilliseconds(INIT_MILLISECONDS + incr * overflow);
		
		int exp = INIT_MILLISECONDS;
		int act = this.time.getMilliseconds();
		assertEquals(exp, act);
		
		exp = INIT_SECONDS + incr;
		act = this.time.getSeconds();
		assertEquals(exp, act);
	}
	
	@Test
	public void testFromMilliseconds() {
		Time exp = this.time;
		Time act = Time.fromMilliseconds(5420400);
		
		assertEquals(exp, act);
	}
	
	@Test
	public void testToMilliseconds() {
		int exp = 5420400;
		int act = this.time.toMilliseconds();
		
		assertEquals(exp, act);
	}
}

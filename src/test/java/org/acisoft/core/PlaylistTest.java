package org.acisoft.core;

import static org.junit.Assert.*;
import static org.junit.Assume.*;

import org.acisoft.core.playable.AudioFilePlayable;
import org.acisoft.core.playable.Playable;
import org.acisoft.core.playable.VideoFilePlayable;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

@Ignore // Tests are failing since the big "Show image" feature refactor
public class PlaylistTest {
	private Playlist playlist;

	@Before
	public void setUp() throws Exception {
		this.playlist = new Playlist();
	}

	@After
	public void tearDown() throws Exception {
		this.playlist = null;
	}
	
	@Test
	public void testAddAudioMediaEntry() {
		assumeTrue(this.playlist.getLength() == 0);
		
		this.playlist.add(new AudioFilePlayable(null));
		assertEquals(1, this.playlist.getLength());
	}

	@Test
	public void testAddVideoMediaEntry() {
		assumeTrue(this.playlist.getLength() == 0);
		
		this.playlist.add(new VideoFilePlayable(null));
		assertEquals(1, this.playlist.getLength());
	}
	
	/*@Test
	public void testRemoveAudioMediaEntry() {
		Playable me = new AudioFilePlayable(null);
		this.playlist.add(me);
		assumeTrue(this.playlist.getLength() == 1);
		
		this.playlist.remove(me);
		assertEquals(0, this.playlist.getLength());
	}*/

	/*@Test
	public void testRemoveVideoMediaEntry() {
		Playable me = new VideoFilePlayable(null);
		this.playlist.add(me);
		assumeTrue(this.playlist.getLength() == 1);
		
		this.playlist.remove(me);
		assertEquals(0, this.playlist.getLength());
	}*/

	@Test
	public void testClear() {
		this.playlist.add(new AudioFilePlayable(null));
		this.playlist.add(new VideoFilePlayable(null));
		assumeTrue(this.playlist.getLength() > 1);
		
		this.playlist.clear();
		assertEquals(0, this.playlist.getLength());
	}
	
	@Test
	public void testGetAtIndex_EmptyListReturnsNull() {
		assertNull(this.playlist.getAtIndex(0));
		assertNull(this.playlist.getAtIndex(-1));
		assertNull(this.playlist.getAtIndex(this.playlist.getLength()));
	}
	
	/*@Test
	public void testGetAtIndex_ValidIndex() {
		Playable m0 = new AudioFilePlayable(null);
		Playable m1 = new VideoFilePlayable(null);
		
		this.playlist.add(m0);
		this.playlist.add(m1);
		assumeTrue(this.playlist.getLength() == 2);
		
		assertEquals(m0, this.playlist.getAtIndex(0));
		assertEquals(m1, this.playlist.getAtIndex(1));
	}*/
	
	@Test
	public void testGetAtIndex_InvalidIndexReturnsNull() {
		Playable m0 = new AudioFilePlayable(null);
		Playable m1 = new VideoFilePlayable(null);
		
		this.playlist.add(m0);
		this.playlist.add(m1);
		assumeTrue(this.playlist.getLength() == 2);
		
		assertNull(this.playlist.getAtIndex(-1));
		assertNull(this.playlist.getAtIndex(this.playlist.getLength()));
	}
	
	/*@Test
	public void testEquals_equal() {
		Playable m0 = new AudioFilePlayable(null);
		Playable m1 = new VideoFilePlayable(null);
		
		this.playlist.add(m0);
		this.playlist.add(m1);
		
		Playlist p2 = new Playlist();
		p2.add(m0);
		p2.add(m1);
		
		assertTrue("Playlist's equal method should return true when two playlist contain the same entries in the same order", this.playlist.equals(p2));
	}*/
	
	@Test
	public void testEquals_notEqualDifferentOrder() {
		Playable m0 = new AudioFilePlayable(null);
		Playable m1 = new VideoFilePlayable(null);
		
		this.playlist.add(m0);
		this.playlist.add(m1);
		
		Playlist p2 = new Playlist();
		p2.add(m1);
		p2.add(m0);
		
		assertFalse("Playlist's equal method should return false when two playlist contain the same entries in a different order", this.playlist.equals(p2));
	}
	
	@Test
	public void testEquals_notEqual_emptyList() {
		Playable m0 = new AudioFilePlayable(null);
		Playable m1 = new VideoFilePlayable(null);
		
		this.playlist.add(m0);
		this.playlist.add(m1);
		
		Playlist p2 = new Playlist();
		
		assertFalse("Playlist's equal method should return false when two playlist contain the same entries in a different order", this.playlist.equals(p2));
	}
	
	@Test
	public void testEquals_notEqual_differentContent() {
		Playable m0 = new AudioFilePlayable(null);
		Playable m1 = new VideoFilePlayable(null);
		
		this.playlist.add(m0);
		this.playlist.add(m1);
		
		Playable m2 = new AudioFilePlayable(null);
		Playable m3 = new VideoFilePlayable(null);
		
		Playlist p2 = new Playlist();
		p2.add(m2);
		p2.add(m3);
		
		assertFalse("Playlist's equal method should return false when two playlist contain the same entries in a different order", this.playlist.equals(p2));
	}
}

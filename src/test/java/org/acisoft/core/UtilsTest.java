package org.acisoft.core;

import static org.junit.Assert.*;

import org.junit.Test;

public class UtilsTest {

	@Test
	public void testValueInRange_validArgs() {
		// Out of range
		assertFalse(Utils.valueInRange(3, 1, 2));
		assertFalse(Utils.valueInRange(-3, -2, -1));
		
		// In range
		assertTrue(Utils.valueInRange(5, 3, 8));
		assertTrue(Utils.valueInRange(-7, -8, -6));
		
		// On minimum or maximum
		assertTrue(Utils.valueInRange(3, 3, 8));
		assertTrue(Utils.valueInRange(-6, -8, -6));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testValueInRange_invalidArgs_throwsException() {
		Utils.valueInRange(1, 6, 4);
	}

	@Test
	public void testRound() {
		double val = 2.7254;
		
		int dec = 3;
		double exp = 2.725;
		double act = Utils.round(val, dec);
		assertEquals(exp, act, 0);
		
		dec = 2;
		exp = 2.73;
		act = Utils.round(val, dec);
		assertEquals(exp, act, 0);
	}

	@Test
	public void testIsRound() {
		assertTrue(Utils.isRound(5415));
		assertFalse(Utils.isRound(57.984));
	}

	@Test
	public void testZeroPrefixNumber_positiveNumbers() {
		long num = 5;
		int len = 3;
		boolean inclMin = false;
		String exp = "005";
		String act = Utils.zeroPrefixNumber(num, len, inclMin);
		assertEquals(exp, act);
		
		num = 195;
		len = 3;
		inclMin = true;
		exp = "195";
		act = Utils.zeroPrefixNumber(num, len, inclMin);
		assertEquals(exp, act);
		
		num = 9716;
		len = 1;
		exp = "9716";
		act = Utils.zeroPrefixNumber(num, len, inclMin);
		assertEquals(exp, act);
	}
	
	@Test
	public void testZeroPrefixNumber_negativeNumbers() {
		long num = -5;
		int len = 3;
		boolean inclMin = false;
		String exp = "-005";
		String act = Utils.zeroPrefixNumber(num, len, inclMin);
		assertEquals(exp, act);
		
		num = -7;
		len = 3;
		inclMin = true;
		exp = "-07";
		act = Utils.zeroPrefixNumber(num, len, inclMin);
		assertEquals(exp, act);
		
		num = -195;
		len = 3;
		inclMin = false;
		exp = "-195";
		act = Utils.zeroPrefixNumber(num, len, inclMin);
		assertEquals(exp, act);
		
		num = -697;
		len = 4;
		inclMin = true;
		exp = "-697";
		act = Utils.zeroPrefixNumber(num, len, inclMin);
		assertEquals(exp, act);
		
		num = -9716;
		len = 1;
		inclMin = false;
		exp = "-9716";
		act = Utils.zeroPrefixNumber(num, len, inclMin);
		assertEquals(exp, act);
	}
	
	@Test
	public void testGetFileExtension_fileNames_withExtension() {
		String arg = "myFile.txt";
		String exp = "txt";
		String act = Utils.getFileExtension(arg);
		assertEquals(exp, act);
		
		arg = "awesomeMedia.mp4";
		exp = "mp4";
		act = Utils.getFileExtension(arg);
		assertEquals(exp, act);
	}
	
	@Test
	public void testGetFileExtension_fileNames_withoutExtension() {
		String arg = "myFile";
		String exp = "";
		String act = Utils.getFileExtension(arg);
		assertEquals(exp, act);
		
		arg = "awesomeMedia";
		exp = "";
		act = Utils.getFileExtension(arg);
		assertEquals(exp, act);
	}
	
	@Test
	public void testGetFileExtension_filePath() {
		String arg = "C:/AWESOME\\Path/to\\myFile.txt";
		String exp = "txt";
		String act = Utils.getFileExtension(arg);
		assertEquals(exp, act);
		
		arg = "/Where\\IS/thaT\\awesomeMedia.mp4";
		exp = "mp4";
		act = Utils.getFileExtension(arg);
		assertEquals(exp, act);
	}
	
	@Test
	public void testGetFileExtension_invalidParameter() {
		String arg = null;
		String exp = "";
		String act = Utils.getFileExtension(arg);
		assertEquals(exp, act);
		
		arg = "";
		//exp = "";
		act = Utils.getFileExtension(arg);
		assertEquals(exp, act);
	}
}

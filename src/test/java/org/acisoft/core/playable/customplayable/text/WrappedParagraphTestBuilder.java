package org.acisoft.core.playable.customplayable.text;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class WrappedParagraphTestBuilder {
    private List<MeasuredText> textLines = new ArrayList<>();
    private int wrappingWidth;

    public WrappedParagraphTestBuilder withTextLine(Consumer<MeasuredTextTestBuilder> consumer) {
        MeasuredTextTestBuilder builder = new MeasuredTextTestBuilder();
        consumer.accept(builder);
        textLines.add(builder.build());
        return this;
    }

    public WrappedParagraphTestBuilder withWrappingWidth(int wrappingWidth) {
        this.wrappingWidth = wrappingWidth;
        return this;
    }

    public WrappedParagraph build() {
        return new WrappedParagraph(textLines, wrappingWidth);
    }
}

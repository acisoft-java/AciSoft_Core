package org.acisoft.core.playable.customplayable.text;

import org.junit.Test;

import java.awt.*;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class WrappedParagraphTest {
    private WrappedParagraph wrappedParagraph;

    @Test
    public void getHeight_returnsTheSumOfAllLineHeightsAndInnerSpacings() throws Exception {
        WrappedParagraph wrappedParagraph = new WrappedParagraphTestBuilder()
                .withWrappingWidth(1000)
                .withTextLine(measuredTextTestBuilder -> measuredTextTestBuilder
                        .withText("")
                        .withWidth(1000)
                        .withHeight(200)
                        .withSpacingAbove(50)
                        .withAscent(20))
                .withTextLine(measuredTextTestBuilder -> measuredTextTestBuilder
                        .withText("")
                        .withWidth(800)
                        .withHeight(100)
                        .withSpacingAbove(100)
                        .withAscent(20))
                .build();

        assertThat(wrappedParagraph.getHeight(), is(400d));
    }

    @Test
    public void calculateLinePositionsCenteredAround_calculatesCorrectOffsets_For1Line() throws Exception {
        WrappedParagraph wrappedParagraph = new WrappedParagraphTestBuilder()
                .withWrappingWidth(1920)
                .withTextLine(measuredTextTestBuilder -> measuredTextTestBuilder
                        .withText("")
                        .withWidth(1000)
                        .withHeight(100)
                        .withSpacingAbove(100)
                        .withAscent(20))
                .build();

        List<PositionedMeasuredText> positionedMeasuredTexts =
                wrappedParagraph.calculateLinePositionsCenteredAround(new Point(0, 0));

        assertThat(positionedMeasuredTexts.get(0).offsetX, is(-500d));
        assertThat(positionedMeasuredTexts.get(0).offsetY, is(-50d));
    }

    @Test
    public void calculateLinePositionsCenteredAround_calculatesCorrectOffsets_For2Lines() throws Exception {
        WrappedParagraph wrappedParagraph = new WrappedParagraphTestBuilder()
                .withWrappingWidth(1920)
                .withTextLine(measuredTextTestBuilder -> measuredTextTestBuilder
                        .withText("")
                        .withWidth(1920)
                        .withHeight(200)
                        .withSpacingAbove(50)
                        .withAscent(20))
                .withTextLine(measuredTextTestBuilder -> measuredTextTestBuilder
                        .withText("")
                        .withWidth(920)
                        .withHeight(100)
                        .withSpacingAbove(100)
                        .withAscent(20))
                .build();

        List<PositionedMeasuredText> positionedMeasuredTexts =
                wrappedParagraph.calculateLinePositionsCenteredAround(new Point(0, 0));

        assertThat(positionedMeasuredTexts.get(0).offsetX, is(-960d));
        assertThat(positionedMeasuredTexts.get(0).offsetY, is(-200d));
        assertThat(positionedMeasuredTexts.get(1).offsetX, is(-460d));
        assertThat(positionedMeasuredTexts.get(1).offsetY, is(100d));
    }
}
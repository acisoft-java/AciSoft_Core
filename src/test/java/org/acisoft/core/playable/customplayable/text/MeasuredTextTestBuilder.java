package org.acisoft.core.playable.customplayable.text;

public class MeasuredTextTestBuilder {
    private MeasuredText measuredText = new MeasuredText();

    public MeasuredTextTestBuilder withText(String text) {
        measuredText.text = text;
        return this;
    }

    public MeasuredTextTestBuilder withWidth(double width) {
        measuredText.width = width;
        return this;
    }

    public MeasuredTextTestBuilder withHeight(double height) {
        measuredText.height = height;
        return this;
    }

    public MeasuredTextTestBuilder withSpacingAbove(double spacingBetweenLines) {
        measuredText.spacingAbove = spacingBetweenLines;
        return this;
    }

    public MeasuredTextTestBuilder withAscent(double ascent) {
        measuredText.ascent = ascent;
        return this;
    }

    public MeasuredText build() {
        return measuredText;
    }
}

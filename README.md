# AciSoft Core

The core of AciSoft containing all core functionalities.

There is, and never will be, a GUI controller in this project (there is a GUI for the embedded player, of course).

## Dependencies
* **vlcj** : https://github.com/caprica/vlcj | Current version: 3.10.1
  * which requires **VLC** : https://www.videolan.org/vlc/ | Current version: 2.2.4 x64
* **SWT** : https://www.eclipse.org/swt/ | Version not important (latest version preferred)